/*
 * @Description: uni-app封装请求对象
 * @Author: long.jiao
 * @Date: 2021-07-13 10:14:41
 */
import config from './config'
import Intercept from './intercept'
import Token from '../../common/token.js'

class Http extends Intercept {
	constructor() {
		super()
		this.opt = {
			...config,
		}
	}

	// 请求方法
	request(url, param, opt) {
		var that = this;
		return this.requestIntercept(opt).then(() => {
			return new Promise((resolve, reject) => {
				//如果是绝对路径，则不拼接基url，否则拼接基url
				url = url.indexOf('http') === 0 ? url : this.opt.baseURL + url;
				uni.request({
					...this.opt,
					...opt,
					url,
					header: {
						...this.opt.header,
						Authorization: Token.getToken(),
						...opt.header,
					},
					data: {
						...this.opt.param,
						...param
					},
					success: function(res) {
						that.resoponeSuccessIntercept(res, opt).then(res => {
							resolve(res)
						}).catch(err => {
							reject(err)
						})

					},
					fail: function(err) {
						that.resoponeFailIntercept(err, opt)
						reject(err)
					}
				})
			})
		})


	}

	get(url, param, opt) {
		opt = {
			isHandleError: true, //默认自动处理错误
			method: 'GET',
			showLoader: true,
			loadText: '加载中',
			...opt
		}

		return this.request(url, param, opt)
	}

	post(url, param, opt) {
		opt = {
			header: {
				'content-type': 'application/json'
			},
			isHandleError: true, //默认自动处理错误
			method: 'POST',
			showLoader: true,
			loadText: '加载中',
			...opt
		}
		return this.request(url, param, opt)
	}
	postForm(url, param, opt) {
		opt = {
			header: {
				'content-type': 'application/x-www-form-urlencoded'
			},
			isHandleError: true, //默认自动处理错误
			method: 'POST',
			showLoader: true,
			loadText: '加载中',
			...opt
		}
		return this.request(url, param, opt)
	}


	/**
	 * doc, xls, ppt, pdf, docx, xlsx, pptx预览
	 */
	downloadFile(url, fileType, opt) {
		opt = {
			// isHandleError: true, //默认自动处理错误
			showLoader: true,
			loadText: '加载中...',
			...opt
		}
		return new Promise((resolve, reject) => {
			const downloadTask = uni.downloadFile({
				url: url.indexOf('http') === 0 ? url : this.opt.baseURL + url,
				header: {
					...this.opt.header,
					...opt.header,
					Authorization: Token.getToken()
				},
				success: (res) => {
					console.log(555, url)
					if (fileType == "png" || fileType == "jpg" || fileType == "jpeg" ||
						fileType == "gif") {
						// 图片预览
						uni.previewImage({
							urls: [res.tempFilePath]
						});
						return
					}else {
						// 文件下载
						let urlx = url.indexOf('http') === 0 ? url+ '&Authorization=' + Token.getToken() : this.opt.baseURL + url+ '&Authorization=' + Token.getToken();
						let dload = document.createElement('a');
						dload.download = ''; // 设置下载的文件名，默认是'下载'
						dload.href = urlx;
						document.body.appendChild(dload);
						dload.click();
						dload.remove();
					}
					
					// uni.openDocument({
						// 文件预览
					// 	filePath: res.tempFilePath,
					// 	fileType: fileType,
					// 	showMenu: true,
					// 	success: function(res) {
					// 		resolve(res)
					// 		console.log('打开成功', res)
					// 	},
					// 	fail: (err) => {
					// 		uni.showToast({
					// 			title: "不支持次格式文件打开",
					// 			duration: 2000,
					// 			icon: "none",
					// 		})
					// 		reject(err)
					// 		console.log('不支持次格式文件打开', err)
					// 	},
					// })
				},
				fail: (err) => {
					uni.showToast({
						title: "下载失败",
						duration: 2000,
						icon: "none",
					})
					console.log("下载失败", err)
				},
			})
		});

	}
}

export default Http;
const HttpInstance = new Http();
export {
	HttpInstance
}
