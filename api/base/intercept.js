/*
 * @Description: 请求拦截
 * @Author: long.jiao
 * @Date: 2021-07-13 10:14:41
 */
import alert from "@/common/alert.js"
import Token from "@/common/token.js"

export default class Intercept {
    constructor() {}

    //请求拦截器 返回promise
    requestIntercept(opt) {
		return new Promise((resolve, reject) => {
		    opt.showLoader && alert.showloading(opt.loadText)
		    resolve()
		})  
    }

    //响应成功拦截器 返回promise
    resoponeSuccessIntercept(res, opt) {
        opt.showLoader && alert.hideloading()
        //获取服务器返回的data和http状态
        const { data, statusCode } = res
        if (data.success == true || data.errcode == '200' || statusCode == '200') {
            return Promise.resolve(data)
        } else if (statusCode == 401) {
            this.loginInvalidHandle()
            return Promise.reject(data)
        } else {
            if (opt.isHandleError) {
                try {
                    alert.toast(data.message || JSON.stringify(data))
                } catch (error) {
                    console.error(error)
                }
            }
            return Promise.reject(data)
        }
    }

    //响应失败拦截器， 比如网络 返回promise
    resoponeFailIntercept(err, opt) {
        console.info("请求失败", err, opt)
        opt.showLoader && alert.hideloading()
        alert.toast("请求失败,网络异常")
        return Promise.reject(err)
    }

    // 401处理
    loginInvalidHandle() {
        uni.showToast({
            title: "暂无权限",
            duration: 2000,
            icon: "none",
        })
    }
}
