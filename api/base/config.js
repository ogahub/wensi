/*
 * @Description: 请求默认配置项
 * @Author: long.jiao
 * @Date: 2021-07-13 10:14:41
 */
import config from '../../config.js';

module.exports = {
    baseURL: config.baseUrl,
	
    header:{
		'Access-Control-Allow-Origin': '*',
		'content-type': 'application/json',
		'accept': 'application/json',
		'clientId': 'pc',
		// 'Authorization': this.getToken() ? ('Bearer '+ this.getToken()) : undefined
    },

    //是否自动将Content-Type为“application/json”的响应数据转化为JSON对象，默认为true   
    parseJson:true,

    //默认参数  
    param:{},

    dataType: 'json',

    responseType: 'text',

    method: 'GET'
}