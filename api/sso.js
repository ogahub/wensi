import Http from "./base/http"

class Sso extends Http {
    constructor() {
        super()
        this.prefix = "/srm/app/iq"
    }
    getLoginInfo(params = {}) {
        return this.get('/gateway/srm/app/user/logininfo',params, {
            showLoader: true
        })
    }
    getLoginSso(params = {}) {
        return this.post('/gateway/console/sys/sysparam/getjson4combobox', params, {
            showLoader: true
        })
    }
    getTicketInfo(params = {}) {
        return this.postForm('/gateway/console/ssologin', params, {
            showLoader: true
        }) 
    }
}

export default new Sso()