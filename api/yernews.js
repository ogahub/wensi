import Http from './base/http'

class Yernews extends Http {
	constructor() {
		super()
		this.prefix = '/srm/app/mini/businessnotice';
	}
	/**
	 * @description: 列表&详情
	 * @param {type} 
	 * @return: 
	 */
	typelist(param = {}) {
		return this.get(this.prefix + "/typelist", param, {
			showLoader: false
		})
	}
	typepage(param = {}) {
		return this.get(this.prefix + '/typepage', param, {
			showLoader: false
		})
	}
	markread(billId) {
		return this.get(this.prefix + '/markread', {
			billId
		}, {
			showLoader: true
		})
	}
	/**
	 * @description: 公告&详情
	 * @param {type} 
	 * @return: 
	 */
	noticelist(param = {}) {
		return this.get('/srm/app/mini/notice/list', param, {
			showLoader: false
		})
	}
	noticeget(id) {
		return this.get('/srm/app/mini/notice/get', {
			id
		}, {
			showLoader: true
		})
	}

}

export default new Yernews()
