import Http from "./base/http"

class Basics extends Http {
    constructor() {
        super()
        this.prefix = "/srm/app/iq"
    }


	workbenchList(param = {}) {
        return this.post("/gateway/workbench/data/gettextdatamorelist", param, {
            showLoader: false
        })
    }
	
	purchasingrequisitionGet(param = {}) {
	    return this.get("/gateway/purchasing/purchasingrequisition/get", param, {
	        showLoader: false,
	    })
	}
	purchasingrequisitionGetdetail(param = {}) {
	    return this.get("/gateway/purchasing/purchasingrequisition/getdetail", param, {
	        showLoader: false,
	    })
	}
	purchasingtopass(param = {}) {
	    return this.post("/gateway/purchasing/purchasingrequisition/topass", param, {
	        showLoader: true,
	    })
	}
	purchasingtonopass(param = {}) {
	    return this.post("/gateway/purchasing/purchasingrequisition/tonopass", param, {
	        showLoader: true,
	    })
	}
	
	
	// 待办获取附件
	purchasinggetall(param = {}) {
	    return this.post("/gateway/console/fs/file/getall", param, {
	        showLoader: true,
			header: {
			    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
			}
	    })
	}
	
	// 询价
	getinquirynoticedtl(param = {}) {
	    return this.post("/gateway/inquiry/inquiry/getinquirynoticedtl", param, {
	        showLoader: false,
	    })
	}
	getinquirymatdtl(param = {}) {
	    return this.post("/gateway/inquiry/inquiry/getinquirymatdtl", param, {
	        showLoader: false,
	    })
	}
	getinquiryvendordtl(param = {}) {
	    return this.post("/gateway/inquiry/inquiry/getinquiryvendordtl", param, {
	        showLoader: false,
	    })
	}
	getbaseInfo(param = {}) {
	    return this.post("/gateway/inquiry/inquiry/get", param, {
	        showLoader: false,
	    })
	}
	inquirytopass(param = {}) {
	    return this.post("/gateway/inquiry/inquiry/topass", param, {
	        showLoader: true,
	    })
	}
	inquirytonopass(param = {}) {
	    return this.post("/gateway/inquiry/inquiry/tonopass", param, {
	        showLoader: true,
	    })
	}
	
	//供应商
	getvendorin(param = {}) {
	    return this.post("/gateway/vendor/vendorin/get", param, {
	        showLoader: false,
	    })
	}
	getvendorincapacityindtl(param = {}) {
	    return this.post("/gateway/vendor/vendorin/findvendorcapacityindtl", param, {
	        showLoader: false,
	    })
	}
	getvendorinqualityindtl(param = {}) {
	    return this.post("/gateway/vendor/vendorin/findvendorqualityindtl", param, {
	        showLoader: false,
	    })
	}
	
	getvendorincompanyindtl(param = {}) {
	    return this.post("/gateway/vendor/vendorin/findvendorcompanyindtl", param, {
	        showLoader: false,
	    })
	}
	getvendorinmaterialgroupindtl(param = {}) {
	    return this.post("/gateway/vendor/vendorin/findvendormaterialgroupindtl", param, {
	        showLoader: false,
	    })
	}
	getvendorinporgindtl(param = {}) {
	    return this.post("/gateway/vendor/vendorin/findvendorporgindtl", param, {
	        showLoader: false,
	    })
	}
	getvendorinauthindtl(param = {}) {
	    return this.post("/gateway/vendor/vendorin/findvendorauthindtl", param, {
	        showLoader: false,
	    })
	}
	getvendorinbankindtl(param = {}) {
	    return this.post("/gateway/vendor/vendorin/findvendorbankindtl", param, {
	        showLoader: false,
	    })
	}
	vendortopass(param = {}) {
	    return this.post("/gateway/vendor/vendorin/topass", param, {
	        showLoader: true,
	    })
	}
	vendortonopass(param = {}) {
	    return this.post("/gateway/vendor/vendorin/tonopass", param, {
	        showLoader: true,
	    })
	}
	
	// 合同
	getcontract(param = {}) {
	    return this.post("/gateway/contract/contract/get", param, {
	        showLoader: false,
	    })
	}
	getcontractMaterial(param = {}) {
	    return this.post("/gateway/contract/contract/listContractMaterial", param, {
	        showLoader: false,
	    })
	}
	getcontractContent(param = {}) {
	    return this.post("/gateway/contract/contract/getContractContent", param, {
	        showLoader: false,
	    })
	}
	getContractField(param = {}) {
	    return this.post("/gateway/contract/contract/listContractField", param, {
	        showLoader: false,
	    })
	}
	// 参数：filter: "(contractNo eq 'HT2021072600009') "
	getContractSignFile(param = {}) {
	    return this.post("/gateway/contract/contract/listContractSignFile", param, {
	        showLoader: false,
	    })
	}
	getContractFile(param = {}) {
	    return this.post("/gateway/contract/contract/listContractFile", param, {
	        showLoader: false,
	    })
	}
	contracttopass(param = {}) {
	    return this.post("/gateway/contract/contract/toPass", param, {
	        showLoader: true,
	    })
	}
	contracttonopass(param = {}) {
	    return this.post("/gateway/contract/contract/toNoPass", param, {
	        showLoader: true,
	    })
	}
	
	
	/**
	 * 文件处理
	 */
	viewFile(param = {}) {
		return this.downloadFile(`/gateway/console/fs/file/viewimage?fileCode=${param.code}`,param.fileType,{
			showLoader: true
		})
	}
	download(param = {}) {
		return this.downloadFile(`/gateway/console/fs/file/download?fileCode=${param.code}`,param.fileType,{
			showLoader: true
		})
	}
	deleteUploadsFile(fileCode) {
		return this.get('/gateway/console/fs/file/deletefile', {fileCode:fileCode}, {
			showLoader: true
		})
	}
	/**
	 * 直采-单
	 */
	
	// contracttonopass(param = {}) {
	//     return this.post("/gateway/contract/contract/toNoPass", param, {
	//         showLoader: false,
	//     })
	// }
	/**
	 * 直采-定价单
	 */
	directpurchaseget(param = {}) {
	    return this.post("/gateway/inquiry/pricing/get", param, {
	        showLoader: false,
	    })
	}
	directpurchasegetdetail(param = {}) {
	    return this.post("/gateway/inquiry/pricing/getdetail", param, {
	        showLoader: false,
	    })
	}
	directpurchasetopass(param = {}) {
	    return this.post("/gateway/inquiry/pricing/topass", param, {
	        showLoader: true,
	    })
	}
	directpurchasetonopass(param = {}) {
	    return this.post("/gateway/inquiry/pricing/tonopass", param, {
	        showLoader: true,
	    })
	}
	
}

export default new Basics()
