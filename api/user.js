import Http from "./base/http"

class User extends Http {
    constructor() {
        super()
        this.prefix = "/srm/wx"
    }
    //获取openId
    getOpenId(param = {}) {
        return this.get(this.prefix + "/getuserinfo", param, { showLoader: true })
    }
    //绑定账号
    bindOpenId(param = {}) {
        return this.get(this.prefix + "/user/bindUser", param, { showLoader: true })
    }
    //解除绑定
    unbind() {
        return this.get(this.prefix + "/user/remoteUser")
    }
    //已绑定-登录
    login(param = {}) {
        return this.get(this.prefix + "/user/login", param, { showLoader: true })
    }
    //修改密码
    changePwd(param = {}) {
        return this.get(this.prefix + "/user/changepwd", param, { showLoader: true })
    }
    //联系方式
    contact(param = {}) {
        return this.get(this.prefix + "/user/getbrandconnect", param, { showLoader: true })
    }
    //操作入门指引下载
    downLoadFile(param = {}) {
        return this.get(this.prefix + "/user/getoperation", param, { showLoader: true })
    }
    //注册
    register(param = {}) {
        return this.get("/srm/app/sl/vendor/register", param, { showLoader: true })
    }
    //注册校验
    registerVerify(param = {}) {
        return this.get("/srm/app/sl/vendor/validate", param, { showLoader: true })
    }
}

export default new User()
