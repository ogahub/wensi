/**
 * 检测表单数据
 * formData: 表单数据{key:value}
 */
function checkForm(formData) {
	console.log('啦啦啦')
	/**
	 * 表单数据对应的效验配置
	 * key 和表单数据的key要相同才能匹配上
	 * reg 为null时，则不核验表单数据该中key的值
	 */
	const VERIFYCONFIG = {
		mobile: {
			title: '手机号',
			reg: /^1(2|3|4|5|6|7|8|9)[0-9]{9}$/g,
		},
		email: {
			title: '邮箱',
			reg: /^[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*@[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*\.[a-z]{2,}$/,
		}
	}

	for (let item in formData) {
		let info = formData[item];
		let reg = VERIFYCONFIG[item] ? VERIFYCONFIG[item].reg : null;
		if (reg) {
			let isValid = reg.test(info);
			reg.lastIndex = 0;
			if (!isValid) {
				uni.showToast({
				    title: VERIFYCONFIG[item].title + '输入错误!',
				    duration: 2000,
				    icon: "none",
				})
				return false
			}
		}
	}
	return true
}



export default {
	checkForm
};
