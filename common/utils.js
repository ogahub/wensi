import config from '@/config';
/**
 * 节流
 */
export const throttle = (func, wait = 1000) => {
	let timeout;
	return function() {
		let context = this;
		let args = arguments;
		if (!timeout) {
			timeout = setTimeout(() => {
				timeout = null;
				func.apply(context, args)
			}, wait)
		}
	}
}

/**
 * 防抖函数
 */
export const debounce = (fn, delay = 600) => {
	// console.log('--',timer)
	var timer;
	return function() {
		var args = arguments;
		if (timer) {
			clearTimeout(timer);
		}
		timer = setTimeout(() => {
			fn.apply(this, args);
		}, delay);
	};
}
/**
 * @function clearDeep 深层copy
 * @return {obj}
 */
export const deepCopy = (obj) => {
	let newObj = {};
	let keys = Object.keys(obj),
		key = null,
		temp = null;
	// console.log(9,keys)	
	for (let i = 0; i < keys.length; i++) {
		key = keys[i];
		temp = obj[key];
		// 如果字段的值也是一个对象则递归操作
		if (temp && typeof temp === 'object') {
			newObj[key] = deepCopy(temp);
		} else {
			// 否则直接赋值给新对象
			newObj[key] = temp;
		}
	}
	return newObj;
}
/**
 * 递归实现深拷贝
 * return cloneObj
 */
function deepCopy2(obj, map = new Map()){
    if(obj === null || typeof obj !== 'object'){
       return obj;
    }
    let cloneObj = map.get(obj);
    if(cloneObj){
        return cloneObj;
    }
    cloneObj = Array.isArray(obj) ? [] : {};
    map.set(obj, cloneObj)
    for(key in obj){
        if(obj.hasOwnProperty(key)){
            if(typeof obj[key] === 'object'){
                cloneObj[key] = deepCopy2(obj[key], map)
            }else{
                cloneObj[key] = obj[key];
            }
        }
    }
    return cloneObj
}

/** 
 * @function dateFormat 格式化日期数据
 * @param {string} dateStr 日期字符串
 */
export const DateParse = (dateStr) => {
	return parseInt(dateStr.replace('-', '/').replace('-', '/'));
}
/**
 * @function autoFillZero 自动补零
 * @param {String|Number} val
 * @param {Number} num
 */
export const autoFillZero = (val, num) => {
	if (isEmpty(val)) {
		return val;
	}
	var pow = Math.pow(10, num);
	var v = Math.round(parseFloat(val) * pow) / pow;
	var xsd = v.toString().split(".");
	if (xsd.length == 1) {
		v = v.toString() + ".";
		v += fillZero(num);
	} else {
		if (xsd[1].length < num) {
			v = v.toString();
			v += fillZero(num - xsd[1].length);
		}
	}
	return v;
}

/**
 * @function fillZero 返回自动补零个数
 * @param {Number} num
 */
export const fillZero = (num) => {
	var chart = '';
	for (var i = 0; i < num; i++) {
		chart += '0';
	}
	return chart;
}
/**
 * @function isEmpty 判断空
 * @param {any} val
 * @return {Boolean}
 */
export const isEmpty = (val) => {
	// null or undefined
	if (val == null) return true;
	if (typeof val === 'boolean') return false;

	if (typeof val === 'number') return !val;

	if (val instanceof Error) return val.message === '';

	switch (Object.prototype.toString.call(val)) {
		// String or Array
		case '[object String]':
		case '[object Array]':
			return !val.length;
			// Map or Set or File
		case '[object File]':
		case '[object Map]':
		case '[object Set]':
			{
				return !val.size;
			}
			// Plain Object
		case '[object Object]':
			{
				return !Object.keys(val).length;
			}
	}

	return false;
}
/**
 * @function isEqual 判断连个对象是否相等
 * @param {Obejct} objA 对象A
 * @param {Obejct} objB 对象B
 */
export const isEqual = (objA, objB) => {
	//相等
	if (objA === objB) return objA !== 0 || 1 / objA === 1 / objB;
	//空判断
	if (objA == null || objB == null) return objA === objB;
	//类型判断
	if (Object.prototype.toString.call(objA) !== Object.prototype.toString.call(objB)) return false;

	switch (Object.prototype.toString.call(objA)) {
		case '[object RegExp]':
		case '[object String]':
			//字符串转换比较
			return '' + objA === '' + objB;
		case '[object Number]':
			//数字转换比较,判断是否为NaN
			if (+objA !== +objA) {
				return +objB !== +objB;
			}

			return +objA === 0 ? 1 / +objA === 1 / objB : +objA === +objB;
		case '[object Date]':
		case '[object Boolean]':
			return +objA === +objB;
		case '[object Array]':
			//判断数组
			for (let i = 0; i < objA.length; i++) {
				if (!isEqual(objA[i], objB[i])) return false;
			}
			return true;
		case '[object Object]':
			//判断对象
			let keys = Object.keys(objA);
			for (let i = 0; i < keys.length; i++) {
				if (!isEqual(objA[keys[i]], objB[keys[i]])) return false;
			}

			keys = Object.keys(objB);
			for (let i = 0; i < keys.length; i++) {
				if (!isEqual(objA[keys[i]], objB[keys[i]])) return false;
			}

			return true;
		default:
			return false;
	}
}

/**
 * @function deepClone 对象或数组的深度克隆
 * @param {Object|Array} obj
 * @returns {Object|Array} newObj
 */

export const deepClone = function(obj) {
	let newObj = Array.isArray(obj) ? [] : {}

	if (obj && typeof obj === "object") {
		for (let key in obj) {
			if (obj.hasOwnProperty(key)) {
				newObj[key] = (obj && obj[key] !== null && typeof obj[key] === 'object') ? deepClone(obj[key]) : obj[key];
			}
		}
	}
	return newObj
}

/**
 * @function clearDeep 深层递归清除空值
 * @return {obj}
 */
export const clearDeep = (obj) => {
	if (!obj || !typeof obj === 'object') {
		return false;
	}
	const keys = Object.keys(obj)
	for (var key of keys) {
		const val = obj[key]
		if (typeof val === 'undefined' || ((typeof val === 'object' || typeof val === 'string') && !val)) {
			// 如属性值为null或undefined或''，则将该属性删除
			delete obj[key]
		} else if (typeof val === 'object') {
			// 属性值为对象，递归调用
			clearDeep(obj[key])
			if (Object.keys(obj[key]).length === 0) {
				// 如某属性的值为不包含任何属性的独享，则将该属性删除
				delete obj[key];
			}
		} else if (typeof val === 'string') {
			obj[key] = val.trim();
		} else {
			obj[key] = val + "";
		}
	}
}

/**
 * @function clearDeepArray 深层清除对象中数组的empty
 * @return {obj}
 */
export const clearDeepArray = (obj) => {
	if (!obj || !typeof obj === 'object') {
		return false;
	}
	if (Array.isArray(obj) && obj.length > 0) {
		let l = obj.length;
		let s = 0;
		for (var i = 0; i < l; i++) {
			if (obj[i] == undefined || obj[i] == null) {
				obj.splice(i - s, 1);
				s++;
			}
		}
	}
	const keys = Object.keys(obj);
	for (var key of keys) {
		let val = obj[key]
		if (typeof val === 'object') {
			clearDeepArray(val);
		}
	}
}
/**
 * 时间转换格式
 */
export const formatDateType = (date, type) => {

	var date = new Date(date);
	Y = date.getFullYear() + '-';
	M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
	D = (date.getDate() < 10 ? '0' + (date.getMonth()) : date.getMonth()) + '-';
	return Y + M + D;

}


/**
 * 时间转换格式
 */
export const formatStatusType = (value) => {
    let o = config.statusMap.find(item => value === item.value)
    return o ? o.label : ''
}
