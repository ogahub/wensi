/**
 * 子应用主动向主应用请求系统数据：如：用户信息，权限，及内置系统参数
 * * @returns {Promise<any>} 
 */
import {APPCONFIG,OPEN_I18N,I18N_DATAS,TOKEN_KEY} from '@/common/constants.js';
import {WHITE_URL_DATA} from '@/config/public-config.js'
import {routeData} from '@/common/link.js';
import i18n from '@/lang';

/**
 * 子应用请求主应用数据
 * @param delay：默认0
 * @returns {Promise<any>} 
 */
export const requestMainAppData = (delay = 0) => {
	//,iframeType=false
	//console.log('log->','start'+getUrlParam('token'))
	return new Promise((resolve, reject) => {
		/*壳的形式子应用请求主应用*/
		if(getUrlParam('token')){
			//console.log('log->','start2')
			/*子页面调用父页面方法*/
			 window.addEventListener('message', function(e) {
			     if(e.source != window.parent) { 
			         return; 
			     } 
			     let result = JSON.parse(e.data);
				 //console.log('log->',result.linkParams)
				 resolve(result);
			 })
		}else{
			let time = null;
			let num = 30;
			let getMainInfo = (time,num)=>{
				let method = 'requestMainAppData';
				if (num > 0) {
					if (window.webkit) {
						webkit.messageHandlers.cordova_iab.postMessage(
							JSON.stringify({
								method,
								args: [method, null]
							})
						);
					}
				} else {
					clearTimeout(time);
					delete window.requestMainAppData;
				}
			} 
			/**
			 * 定义申明与主应用交互的函数
			 */
			window.requestMainAppData = (successful, res) => {
				//console.log('数据词条',res)
				/*清除定时器*/
				let result = JSON.parse(res);
				if(result.hasOwnProperty('linkParams')&&Object.keys(result.linkParams)){
					if(time){
						clearTimeout(time);
					}
					delete window.requestMainAppData;
					resolve(result);
				}
			};
			setTimeout(() => {
                getMainInfo(time,num);
				time = setInterval(() => {
					num--;
					getMainInfo(time,num);
				}, 200);
			}, delay)
		}
	});
}

// #ifdef  H5
	 const loadJs = (url) => {
	  return new Promise((resolve, reject) => {
		const script = document.createElement('script')
		script.src = url
		script.type = 'text/javascript'
		document.body.appendChild(script)
		script.onload = () => {
		  resolve()
		}
	  }) 
	}
	// #endif
/**
 * 
 * @param {openi18n} boolean：默认false
 * @returns {Promise<any>} 
 */
export const asyncLoadLang = (languageKey = 'zh_CN') => {
	/*国际化未开启，或者国际化状态为中文*/
	return new Promise((resolve, reject) => {
		if (!OPEN_I18N || languageKey == 'zh_CN') {
			resolve(5);
		} else {
			window.getI18nData = function(successful,data){
				//alert(data)
				let languageData = JSON.parse(data);
				handleI8nData(languageKey,languageData);
				resolve(0);
			}
			/*请求主应用国际化*/
			if (window.webkit) {
				let method = 'getI18nData';
				webkit.messageHandlers.cordova_iab.postMessage(
					JSON.stringify({
						method,
						args: [method, null]
					})
				);
			}else{
				loadJs('./static/en.js').then(result => {
					let languageData = JSON.parse(enData.data);
					handleI8nData(languageKey,languageData);
					//国际化
					resolve(0);
				});
			}
		
		}
	})
}
 /**
  * 处理多语言
  * @param {Object} languageKey
  * @param {Object} languageData
  */
function handleI8nData(languageKey,languageData){
	let obj = {};
	let localeLang = i18n.getLocaleMessage(languageKey);
	let newLang = Object.assign(obj, localeLang, languageData);
	i18n.setLocaleMessage(languageKey, newLang);
	i18n.locale = languageKey;
}
/**
 * 
 * @param {openi18n} boolean：默认false
 * @returns {Promise<any>} 
 */
export const handleResult = (result) => {
	if (result) {
		let {
			token,
			user,
			language,
			opVersion,
			url: baseUrl,
			moduleAuth,
			linkParams,
			linkParams: {
				action,
				moduleCode,
				secondUrl,
				otherUrl
			}
		} = result;
		if(user&&token){
			let {
				userCode,
				customerCorpNo
			} = user;
			Object.assign(APPCONFIG, {
				userCode,
				customerCorpNo,
				token
			})
		}
		Object.assign(APPCONFIG, {
			baseUrl,
			language,
			opVersion
		})
		//Vue.prototype.$opVersion = opVersion;
		/**处理页面跳转**/
		if (linkParams && action && moduleCode) {
			let routeName = '';
			let baseUrl = '';
			if (otherUrl) {
				routeName = otherUrl;
			} else {
				/**处理路由**/
				let resultRoute = routeData.find(item => {
					let value = item.value;
					if (Array.isArray(value)) {
						return value.some(code => {
							return code == moduleCode
						})
					} else {
						return value == moduleCode
					}
				})
				if (resultRoute) {
					let {
						link,
						base
					} = resultRoute;
					baseUrl = base;
					routeName = link[action] ? `${link[action]}` : secondUrl;
				} else if (secondUrl) {
					routeName = secondUrl;
				}

			}
			if (routeName) {
				delete linkParams.moduleCode;
				delete linkParams.action;
				delete linkParams.secondUrl;
				if (linkParams.hasOwnProperty('SCENE_INFO')) {
					delete linkParams.SCENE_INFO;
				}
				if (routeName && routeName !== 'pages/') {
					if (routeName.indexOf('pages/') != -1) {
						uni.reLaunch({
							url: `${routeName}` + encode(linkParams)
						});
					} else {
						routeName = `/pages/${baseUrl}/${routeName}` + encode(linkParams);
						uni.reLaunch({
							url: routeName
						});
					}
				} else {
					showError();
				}
			} else {
				showError();
			}
		}else if(otherUrl){
			uni.reLaunch({
				url: `/pages/${otherUrl}`
			});
		}else{
			showError();
		}
	} else {
		window.closeInBrowser({});
	}
}
/**
 * @param {key} 
 * 截取url地址后面的参数
 */
export const getUrlParam = (key) => {
	var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null) {
		return decodeURI(r[2]);
	}
	return null;
}
/**
 *  白名单url
 * @param {Object} url
 * 
 */
export const hasExisteWhite = (url) => {
	return WHITE_URL_DATA.some(item => {
		return item == url;
	})
}

/*处理全局国际化*/
export const handleGlobalI18n = (self) =>{
	for (let key in I18N_DATAS) {
		I18N_DATAS[key] = self.$t(`qscLocal.common.${key}`);
	}
}

/**
 * @param {params} 
 * 对请求对参数进行编码处理
 */
const encode = (params) => {
	var str = '';
	if (params) {
		var flag = true;
		for (var key in params) {
			flag = false;
			if (params.hasOwnProperty(key)) {
				var value = params[key];
				str += encodeURIComponent(key) + '=' + encodeURIComponent(value) + '&';
			}
		}
		if (!flag) { //不是空对象时
			str = '?' + str.substring(0, str.length - 1);
		}
	}
	return str;
}


/**
 * 显示错误信息
 */
function showError() {
	let content = '模块编码配置错误，当前页面不可访问！';
	uni.showModal({
		content: content,
		success: function(res) {
			if (res.confirm) {
				window.closeInBrowser({});
			} else if (res.cancel) {
				window.closeInBrowser({});
			}
		}
	});
}
