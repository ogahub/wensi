import {
	DATA_DICTIONARY
} from '@/common/constants';
import {
	autoFillZero
} from '@/common/utils.js';
import {
	get,
	post,
	submit,
	show,
	upload
} from './request';
/**
 * 
 * @param data 
 * @param json 
 */
const handleResult = (data, json) => {
	/*数据类型判断*/
	// console.log(data);
	if (!Array.isArray(data)) {
		if (data.hasOwnProperty('data')) {
			data = data.data;
			if (data && !Array.isArray(data)) {
				var key = ['records', 'record', 'result'].find(function(item) {
					return data.hasOwnProperty(item)
				})
				if (key) {
					data = data[key];
				}
			}
		} else {
			var key = ['records', 'record', 'result', "company"].find(function(item) {
				return data.hasOwnProperty(item)
			})
			if (key) {
				data = data[key];
			}
		}
	}
	if (!json || Array.isArray(json)) {
		return data;
	}
	var keys = Object.keys(json);
	return (data || []).map(item => {
		var temp = {};
		keys.forEach(key => {
			if (item.hasOwnProperty(json[key])) {
				temp[key] = item[json[key]];
			}
		})
		return temp;
	})
}
/**
 * 
 * @param date 
 * @param fmt 
 */
const formatData = (date, fmt) => {
	if (date && date != '-') {
		if (typeof(date) == 'string' && /^-?\d+$/.test(date)) {
			date = date - 0;
			date = new Date(date);
		} else if (typeof(date) == 'string') {
			var reg = /^[0-9]+.?[0-9]*$/;
			if (date.length == 10) {
				date = date + ' 00:00:00';
			}
			if (!reg.test(date)) {
				let arr = date.split(/[- :]/);
				date = new Date(Number(arr[0]), Number(arr[1]) - 1, Number(arr[2]), Number(arr[3]), Number(arr[4]), Number(arr[5]));
			}
		} else if (typeof(date) == 'number') {
			date = new Date(date);
		}
		fmt = fmt ? fmt : 'yyyy-MM-dd';
		if (/(y+)/.test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
		}
		let o = {
			'M+': date.getMonth() + 1,
			'd+': date.getDate(),
			'h+': date.getHours(),
			'm+': date.getMinutes(),
			's+': date.getSeconds()
		}
		for (let k in o) {
			if (new RegExp(`(${k})`).test(fmt)) {
				let str = o[k] + ''
				fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : (('00' + str).substr(str.length)))
			}
		}
		return fmt
	} else {
		return '-';
	}
}
/*日期格式*/
const timeDisplay = (oldTime, systemTime) => {
	if (!systemTime) {
		systemTime = new Date().getTime();
	}
	if (typeof(oldTime) != 'undefined' && oldTime != '' && oldTime != '---' && oldTime) {
		let arr = [];
		if ((oldTime + '').indexOf(":") != -1) {
			arr = oldTime.split(/[- :]/);
		} else {
			arr[0] = oldTime;
		}
		let _oldStamp = "";
		if (arr.length > 1) {
			_oldStamp = new Date(Number(arr[0]), Number(arr[1]) - 1, Number(arr[2]), Number(arr[3]), Number(arr[4]), Number(
				arr[5]));
		} else {
			_oldStamp = new Date(Number(arr[0]));
		}
		var nowDate = new Date(systemTime).getDate(); //获取当前日期
		var nowMouth = new Date(systemTime).getMonth() + 1; //获取当前月份
		var nowYear = new Date(systemTime).getFullYear(); //获取当前年份
		var nowDay = new Date(systemTime).getDay(); //当前星期
		if (nowDay == 0) { //周日
			nowDay += 7;
		}
		/*消息的日期*/
		var msgYear = new Date(_oldStamp).getFullYear(),
			msgDate = new Date(_oldStamp).getDate(),
			msgMouth = new Date(_oldStamp).getMonth() + 1,
			msgDay = new Date(_oldStamp).getDay();
		//同一年
		if ((nowYear - msgYear) == 0) {
			/*同一月*/
			if ((nowMouth - msgMouth) == 0) {
				if (nowDate - msgDate == 0) {
					/*同一天*/
					return formatData(_oldStamp, 'hh:mm');
				} else if (nowDay > 1 && (nowDate - msgDate) > 0 && (nowDate - msgDate) < 7) {
					/*同一周内*/
					if (nowDay == 2 && (nowDate - msgDate) == 1) { //周二
						return getWeekDay(msgDay);
					} else if (nowDay == 3 && (nowDate - msgDate) <= 2) { //周三
						return getWeekDay(msgDay);
					} else if (nowDay == 4 && (nowDate - msgDate) <= 3) { //周四
						return getWeekDay(msgDay);
					} else if (nowDay == 5 && (nowDate - msgDate) <= 4) { //周五
						return getWeekDay(msgDay);
					} else if (nowDay == 6 && (nowDate - msgDate) <= 5) { //周六
						return getWeekDay(msgDay);
					} else if (nowDay == 7 && (nowDate - msgDate) <= 6) { //周日
						return getWeekDay(msgDay);
					} else if ((nowDay <= msgDay) || msgDay == 0) { //当月
						return formatData(_oldStamp, 'MM/dd');
					}
				} else {
					return formatData(_oldStamp, 'MM/dd');
				}
			} else { //同年不同月
				return formatData(_oldStamp, 'MM/dd');
			}
		} else { //不同年
			return formatData(_oldStamp, 'yyyy/MM/dd');
		}
	} else {
		return "";
	}
}
/*匹配日期*/
const getWeekDay = (week) => {
	if (week == 0) {
		return '星期日';
	} else if (week == 1) {
		return '星期一';
	} else if (week == 2) {
		return '星期二';
	} else if (week == 3) {
		return '星期三';
	} else if (week == 4) {
		return '星期四';
	} else if (week == 5) {
		return '星期五';
	} else if (week == 6) {
		return '星期六';
	}
}
/**
 * 
 * @param groupCode 
 * @param code 
 * @param request
 */
const getDictionary = (groupCode, code, post, store) => {
	return new Promise((resolve, reject) => {
		var state = store.state;
		if (state.basics.dictionary.hasOwnProperty(groupCode) && state.basics.dictionary[groupCode].length != 0) {
			if (Array.isArray(code)) {
				resolve(state.dictionary[groupCode]);
			}
			let disctionary = (state.dictionary[groupCode]).find((res) => {
				return res.itemCode == code;
			})
			resolve(code != '' ? (disctionary != null ? disctionary.itemName : '') : '');
		} else {
			post(DATA_DICTIONARY, {
				groupCode: groupCode
			}).then(res => {
				res = handleResult(res);
				if (Array.isArray(res)) {
					state.dictionary[groupCode] = [];
					res.forEach(item => {
						let temp = {
							itemCode: item.itemCode,
							itemName: item.itemName
						}
						Object.freeze(temp);
						state.dictionary[groupCode].push(temp);
						if (!Array.isArray(code) && code != '' && item.itemCode == code) {
							resolve(item.itemName);
						}
					})
					if (Array.isArray(code)) {
						resolve(state.dictionary[groupCode]);
					}
					if (code == '') {
						resolve('');
					}
				}
			}, err => {
				reject(err)
			})
		}
	})
}
/**
 * @param data 
 * @param data 
 * @store
 */
const formatListData = (data, templateData, post, store, targetKey, isFree) => {
	let newData = [];
	if (!data) {
		return [];
	}

	if (!Array.isArray(data)) {
		data = [data];
	}
	data.forEach((itemData, index) => {
		/*指定的对象处理*/
		if (targetKey) {
			if (itemData.hasOwnProperty(targetKey)) {
				itemData = itemData[targetKey];
			}
		}
		let myDatas = {
			list: []
		};
		let tempData = [];
		templateData.forEach((item, subIndex) => {
			let keys = Object.keys(item);
			let itemValue = "";
			let temp_key = keys[0];
			let temp_item = {};
			let pro = item[temp_key];
			if (temp_key == 'uploadFile') {
				//处理附件组件
				// console.log('pro',pro);
				let uploadFile = [];
				if (Array.isArray(pro)) {
					pro.forEach(subItem => {
						let u_key = Object.keys(subItem);
						uploadFile.push(Object.assign({
							itemValue: itemData[u_key]
						}, subItem[u_key]));
					})
				}
				myDatas['uploadFile'] = uploadFile;
			} else if (temp_key != "others") {
				/** */
				let temp_key_ =
					temp_key.indexOf(".") != -1 ? temp_key.split(".") : [];
				/*时间格式处理*/
				if (pro.hasOwnProperty("format")) {
					let temp_time = temp_key_.length > 0 ?
						itemData[temp_key_[0]][temp_key_[1]] :
						itemData[temp_key];
					itemValue = formatData(temp_time, pro.format);
				} else if (pro.hasOwnProperty("dict")) {
					itemValue = itemData[temp_key];
					// getDictionary(pro.dict, itemData[temp_key], submit, store).then(res => {
					// 	newData[index].list[subIndex]['itemValue1'] = res;
					// }, err => {
					// 	newData[index].list[subIndex].itemValue = itemData[temp_key];
					// })
				} else if (pro.hasOwnProperty("num")) {
					//小数点格式化
					itemValue = autoFillZero(itemData[temp_key], pro.num);
				}else {
					let temp_item_ = [];
					if (temp_key.indexOf('-') != -1) {
						temp_item_ = temp_key.split('-');
					}
					itemValue = temp_key_.length > 0 ?
						itemData[temp_key_[0]][temp_key_[1]] :
						(temp_item_.length > 0 ? (itemData[temp_item_[0]] ? itemData[temp_item_[0]] : itemData[temp_item_[1]]) :
							itemData[temp_key]);
				}
				/*附加属性*/
				temp_item.itemName = pro.name;
				temp_item.itemValue = itemValue;
				if (pro.hasOwnProperty('key') && itemData.hasOwnProperty(pro.key)) {
					temp_item['key'] = itemData[pro.key];
				}
				// 前缀：如 $400.00
				if (pro.hasOwnProperty('extendKey1') && itemData.hasOwnProperty(pro.extendKey1)) {
					temp_item.extendKey1 = itemData[pro.extendKey1];
				}
				//后缀
				if (pro.hasOwnProperty('extendKey2') && itemData.hasOwnProperty(pro.extendKey2)) {
					temp_item.extendKey2 = itemData[pro.extendKey2];
				}
				Object.keys(pro).forEach(pro_key => {
					if (!/key|name|extendKey1|extendKey2/.test(pro_key)) {
						temp_item[pro_key] = pro[pro_key];
					}
				})
				tempData.push(temp_item);
			} else {
				/**是否包含二级属性 */
				item.others.forEach(otherKey => {
					if (otherKey.indexOf('>') != -1) {
						let otherKeys = otherKey.split('>');
						let [key, dict, newField] = otherKeys;
						let oterValue = itemData[key];
						if (oterValue) {
							// getDictionary(dict, oterValue, submit, store).then(res => {
							// 	myDatas[newField] = res;
							// })
						} else {
							myDatas[otherKey] = oterValue;
						}

					} else if (otherKey.indexOf(".") != -1) {
						let sub_pro = otherKey.split(".");
						if (sub_pro.length >= 2) {
							if (myDatas.hasOwnProperty(sub_pro[0])) {
								myDatas[sub_pro[0]][sub_pro[1]] =
									itemData[sub_pro[0]][sub_pro[1]];
							} else {
								myDatas[sub_pro[0]] = {};
								myDatas[sub_pro[0]][sub_pro[1]] =
									itemData[sub_pro[0]][sub_pro[1]];
							}
						}
					} else {
						/*判断是否有数据字典*/
						myDatas[otherKey] = itemData[otherKey];
					}
				});
			}
			if (Array.isArray(tempData)) {
				tempData.forEach(item => {
					//console.log('item',item)
					if (!item.hasOwnProperty("dict") && !item.hasOwnProperty("dateKey") && !item.hasOwnProperty("pay")) {
						if (isFree) {
							Object.freeze(item);
						}
					}
				})
			}
		});
		//日期格式化
		// if(myDatas.hasOwnProperty('createTime')&&myDatas.createTime){
		// 	myDatas.createTime = timeDisplay(myDatas.createTime)
		// }
		myDatas.list = tempData;
		if (!itemData.hasOwnProperty('select')) {
			if (isFree) {
				Object.freeze(myDatas);
			}
		}
		newData.push(myDatas);
	});
	return newData;
}
/*数据展示过滤*/
/**
 * data-form是config配置项
 * item是请求下来的数据
 * 如果是data-from，需要在组件中配置  :dataObj="dtl"
 * <custome-data-form :ref="'materialInfo_'+index" :config="dtl.list" :dataObj="dtl"  :show="dtl.isOpen">
 * 
 */
const customValue = (item, data, type) => {
	if (!item.hasOwnProperty('itemValue')) {
		//form-赋值
		item.itemValue = data[item.value];
	}
	// console.log(item, data, type);
	if (item.itemValue && item.itemValue != '-') {
		let isDecimal = item.decimal ? true : false;
		if (type == 'unit-num') {
			return (item.decimal?item.itemValue.toFixed(item.decimal):item.itemValue) + `${data[item.unit]}`
		} else if (type == 'percent') {
			return item.itemValue * 100 + '%';
		} else if (type == 'unit-price') {
			let price=item.decimal?parseFloat(item.itemValue).toFixed(item.decimal):item.itemValue;
			return `${data.currencySymbol||filterCurrencySymbol(data.currencyCode||'')}` +price +
				`/${data[item.baseUnit]=='1'?'':(data[item.baseUnit]?data[item.baseUnit]:'')}${data[item.unit]}`
		} else if (type == 'money') {
			let price=item.decimal?parseFloat(item.itemValue).toFixed(item.decimal):item.itemValue;
			return `${data.currencySymbol||filterCurrencySymbol(data.currencyCode||'')}` + price;
		} else {
			return item.itemValue;
		}
	} else {
		return '-';
	}

}
/**
 * @param {Object} arr传入要过滤的数据字典数组
 */
const getSetDic = (arr = [], code = '', submit, store) => {
	if (!Array.isArray(arr)) {
		// console.info('$getSetDic--只能传入数组')
		return;
	}
	let sessionDicObj = JSON.parse(sessionStorage.getItem('dictionary'));
	let obj = {};

	arr.forEach(groupCode => {
		if (sessionDicObj && sessionDicObj[groupCode] && sessionDicObj[groupCode].length > 0) {
			return;
		}
		getDictionary(groupCode, code, submit, store).then(res => {
			obj[groupCode] = res;
			sessionStorage.setItem('dictionary', JSON.stringify(obj));
		});
	});
}
/**
 * 获取货币符号
 * 
 */
const getCurrencySymbol = () => {
	// let storageValue=uni.getStorageSync('currencySymbolList');
	submit('masterdata/findcurrencyall', {}).then(res => {
		 uni.setStorageSync('currencySymbolList',  JSON.stringify(res.data));
	});
}
/**
 * 通过货币code找出货币符号
 */
const filterCurrencySymbol = (currencyCode) => {
	let list = JSON.parse(uni.getStorageSync('currencySymbolList'));
	if (!list || list.length == 0) {
		return '';
	}
	let item = list.filter(it => {
		return it.currencyCode == currencyCode;
	})
	// alert(JSON.stringify(item))
	return item[0] ? item[0].currencySymbol : '';
}



module.exports = {
	handleResult: handleResult,
	formatData: formatData,
	getDictionary: getDictionary,
	formatListData: formatListData,
	customValue: customValue,
	getSetDic: getSetDic,
	getCurrencySymbol: getCurrencySymbol,
	filterCurrencySymbol: filterCurrencySymbol
}
