export const billStatusDictionary = [];
/**
 * 扩展的状态编码
 */
let extendStatu = {
	// 业务模块异常信息状态匹配
	moduleExceptionDictionary: [{
		label: '驳回',
		value: 'toNoPass'
	}, {
		label: '拒绝',
		value: 'refuse'
	}, {
		label: '取消',
		value: 'cancel'
	}, {
		label: '采购废标',
		value: 'abolish'
	}, {
		label: '终止',
		value: 'end'
	}, {
		label: '拒绝',
		value: 'rejectToB'
	}, {
		label: '拒绝',
		value: 'rejectToV'
	}],
	"CGD": [{
		label: '新建',
		value: 'NEW',
		css: 'normal'
	}, {
		label: '发布',
		value: 'RELEASE',
		css: 'normal'
	}, {
		label: '执行',
		value: 'OPEN',
		css: 'normal'
	}, {
		label: '关闭',
		value: 'CLOSE',
		css: 'normal'
	}, {
		label: '取消',
		value: 'CANCEL',
		css: 'neutral'
	}],
	"XJD": [{
		label: '询价中',
		value: 'INQUIRYING',
		css: 'emphasize'
	}, {
		label: '询价取消',
		value: 'INQUIRYCANCEL',
		css: 'neutral'
	}, {
		label: '询价截止',
		value: 'INQUIRYSTOP',
		css: 'normal'
	}, {
		label: '定价中',
		value: 'PRICING',
		css: 'normal'
	}, {
		label: '定价完成',
		value: 'PRICINGEND',
		css: 'normal'
	}, {
		label: '待确认',
		value: 'CONFIRMOVER',
		css: 'normal'
	}, {
		label: '议价中',
		value: 'BARGAINING',
		css: 'normal'
	}],
	"ZBD": [{
			label: '投标中',
			value: 'TOTENDER',
			css: 'emphasize'
		}, {
			label: '待投标',
			value: 'TOBID'
		}, {
			label: '待开标',
			value: 'TOOPEN',
			css: 'normal'
		}, {
			label: '评标中',
			value: 'EVALING'
		}, {
			label: '待定标',
			value: 'TOSURE'
		}, {
			label: '定标中',
			value: 'SCALING'
		}, {
			label: '定标完成',
			value: 'CLOSESCALING'
		}, {
			label: '废标',
			value: 'TOABOLISH',
			css: 'neutral'
		}, {
			label: '新建',
			value: 'NEW'
		}, {
			label: '待审核',
			value: 'CONFIRM'
		}, {
			label: '驳回',
			value: 'REJECT',
			css: 'warning'
		}, {
			label: '待审核过期',
			value: 'CONFIRMOVER'
		}, {
			label: '待定标',
			value: 'TOSURE'
		}, {
			label: '定标完成',
			value: 'CLOSESCALING'
		},
		{
			label: '拒绝',
			value: 'REJECTTOV',
			css: 'warning'
		}


	],
	"HT": [{
			label: '新建',
			value: 'NEW',
			css: 'emphasize'
		}, {
			label: '发布',
			value: 'RELEASE',
			css: 'warning'
		}, {
			label: '待确认',
			value: 'UNCONFIRM',
			css: 'emphasize'
		}, {
			label: '待审核',
			value: 'CONFIRM',
			css: 'normal'
		}, {
			label: '拒绝',
			value: 'REFUSE',
			css: 'warning'
		}, {
			label: '驳回',
			value: 'REJECT',
			css: 'warning'
		},
		{
			label: '待签订',
			value: 'UNSING',
			css: 'normal',
			secondCss: 'emphasize'
		}, {
			label: '生效',
			value: 'EFFECTIVE',
			css: 'normal'
		}, {
			label: '到期',
			value: 'MATURITY',
			css: 'normal'
		}, {
			label: '终止',
			value: 'TERMINATION',
			css: 'warning'
		}, {
			label: '完成',
			value: 'COMPLETE',
			css: 'normal'
		}
	],
	"ASN": [{
		label: '新建',
		value: 'NEW',
		css: 'normal'
	}, {
		label: '待收货',
		value: 'WAIT',
		css: 'normal'
	}, {
		label: '收货中',
		value: 'RECEIVING',
		css: 'normal'
	}, {
		label: '收货完成',
		value: 'CLOSE',
		css: 'normal',
	}, {
		label: '取消',
		value: 'CANCEL',
		css: 'neutral'
	}],
	"YPD": [{
		label: '新建',
		value: 'NEW',
		css: 'emphasize'
	}, {
		label: '发布',
		value: 'RELEASE',
		css: 'normal'
	}, {
		label: '拒绝',
		value: 'REJECT',
		css: 'warning'
	}, {
		label: '执行',
		value: 'OPEN',
		css: 'process'
	}, {
		label: '关闭',
		value: 'SHUT',
		css: 'neutral'
	}, {
		label: '取消',
		value: 'CANCEL',
		css: 'neutral'
	}, {
		label: '完成',
		value: 'CLOSE',
		css: 'normal'
	}],
	"SYD": [{
		label: '新建',
		value: 'NEW',
		css: 'process'
	}, {
		label: '待收货',
		value: 'UNACCEPT',
		css: 'normal'
	}, {
		label: '待检验',
		value: 'UNCHECK',
		css: 'normal'
	}, {
		label: '取消',
		value: 'CANCEL',
		css: 'neutral'
	}, {
		label: '完成',
		value: 'CLOSE',
		css: 'normal'
	}],
	"JJD": [{
		label: '新建',
		value: 'NEW',
		css: 'normal'
	}, {
		label: '待审核',
		value: 'CONFIRM',
		css: 'normal'
	}, {
		label: '发布',
		value: 'RELEASE',
		css: 'emphasize'
	}, {
		label: '取消',
		value: 'CANCEL',
		css: 'neutral'
	}, {
		label: '竞价中',
		value: 'BIDDING',
		css: 'emphasize'
	}, {
		label: '竞价截止',
		value: 'BIDDINGSTOP',
		css: 'normal'
	}, {
		label: '定标中',
		value: 'SCALING',
		css: 'normal'
	}, {
		label: '完成',
		value: 'COMPLETE',
		css: 'normal'
	}],
	"ZGTZ": [{
			label: '方案待回复',
			value: 'TOREPLYSCHEME',
			css: 'emphasize'
		},
		{
			label: '方案已驳回',
			value: 'PROPOSALREJECT',
			css: 'warning'
		}, {
			label: '方案待确认',
			value: 'TOAFFIRMSCHEME',
			css: 'emphasize'
		}, {
			label: '整改中',
			value: 'CORRECTIVING',
			css: 'normal'
		}, {
			label: '整改完成',
			value: 'CLOSE',
			css: 'normal'
		}, {
			label: '取消',
			value: 'CANCEL',
			css: 'neutral'
		}, {
			label: '整改待验收',
			value: 'TOAFFIRMRESULT',
			css: 'normal'
		}
	],
	"FEU": [{
		label: '新建',
		value: 'NEW',
		css: 'normal'
	}, {
		label: '发布',
		value: 'RELEASE',
		css: 'normal'
	}, {
		label: '执行',
		value: 'OPEN',
		css: 'normal'
	}, {
		label: '关闭',
		value: 'CLOSE',
		css: 'normal'
	}, {
		label: '取消',
		value: 'CANCEL',
		css: 'neutral'
	}],
	"XXBG":[
		{
			label: "新建", 
			value: "NEW",
			css: 'normal'
		}, {
			label: "待审核",
			value: "CONFIRM",
			css: 'emphasize'
		}, {
			label: "驳回", 
			value: "TONOPASS",
			css: 'warning'
		}, {
			label: "完成", 
			value: "TOPASS",
			css: 'normal'
		}, {
			label: "驳回", 
			value: "REJECT",
			css: 'warning'
		}
	],
	"HZSQ":[
		{
			name:"新供应商",
			value:'NEW',
			css: 'normal'
		},{
			name:"注册供应商",
			value:'REGISTRATION',
			css: 'normal'
		},{
			name:"潜在供应商",
			value:'POTENTIAL',
			css: 'normal'
		},{
			name:"合格供应商",
			value:'QUALIFIED',
			css: 'normal'
		}
	],
	"DZD": [{
		label: '新建',
		value: 'NEW',
		css: 'normal'
	}, {
		label: '待审核',
		value: 'CONFIRM',
		css: 'normal'
	}, {
		label: '驳回',
		value: 'NOPASS',
		css: 'warning'
	}, {
		label: '待供应商确认',
		value: 'RELEASE',
		css: 'emphasize'
	}, {
		label: '拒绝',
		value: 'REFUSE',
		css: 'warning'
	}, {
		label: '待开票',
		value: 'OPEN',
		css: 'emphasize'
	}, {
		label: '待过账',
		value: 'TOPOST',
		css: 'emphasize'
	}, {
		label: '取消',
		value: 'CANCEL',
		css: 'neutral'
	}, {
		label: '开票中',
		value: 'INVOICEING',
		css: 'normal'
	}, {
		label: '开票完成',
		value: 'INVOICESUCCESS',
		css: 'normal'
	}],
	"FP": [{
		label: '新建',
		value: 'NEW',
		css: 'normal'
	}, {
		label: '待审核',
		value: 'CONFIRM',
		css: 'normal'
	}, {
		label: '驳回',
		value: 'NOPASS',
		css: 'warning'
	}, {
		label: '驳回',
		value: 'REJECT',
		css: 'warning'
	}, {
		label: '待过账',
		value: 'TOPOST',
		css: 'emphasize'
	}, {
		label: '待过账',
		value: 'OPEN',
		css: 'emphasize'
	}, {
		label: '冲销',
		value: 'WRITEOFF',
		css: 'warning'
	}, {
		label: '已过账',
		value: 'POSTED',
		css: 'emphasize'
	}, {
		label: '取消',
		value: 'CANCEL',
		css: 'neutral'
	}],
	// 对账单类型
	"DZDTYPE": [{
		label: '寄售',
		value: 'K',
	}, {
		label: '寄售退货',
		value: 'OK',
	}, {
		label: '标准退货',
		value: 'OS',
	}, {
		label: '标准',
		value: 'S',
	}],
	// 电子签进度
	"DZPRogress": [{
		label: '需签署',
		value: 'NEED_SIGN',
	}, {
		label: '签署失败',
		value: 'SIGN_FAIL',
	}, {
		label: '已签署',
		value: 'SIGN_SUCCESS',
	}, {
		label: '待签署',
		value: 'WAIT_SIGN',
	}],
	"IMPROVE":[{
		label:"qsc.label.name.questionsRaised", 
		value: 'D0'
	},{
		label:"qsc.label.name.groupSetUp", 
		value: 'D1'
	},{
		label:"qsc.label.name.problemDefinition", 
		value: 'D2'
	},{
		label:"qsc.label.name.containmentAction", 
		value: 'd3'
	},{
		label:"qsc.label.name.causeAnalysis", 
		value: 'd4'
	},{
		label:"qsc.label.name.correctiveAction",
		value: 'd5'
	},{
		label:"qsc.label.name.effectVerification",
		value: 'd6'
	},{
		label:"qsc.label.name.preventRecurrence",
		value: 'd7'
	},{
		label:"qsc.label.name.closingEvaluation",
		value: 'd8'
	},{
		label:"qsc.label.caseClose", 
		value: 'FINISH'
	}]
};
export default {
	install: function(Vue) {
		Vue.prototype.getClassName = function(statusCode, moduleCode, secondCode) {
				let billStatusDictionary_ = billStatusDictionary;
				if (moduleCode) {
					billStatusDictionary_ = extendStatu[moduleCode];
				}
				let result = billStatusDictionary_.find((item) => {
					return item.value == statusCode;
				})
				if (secondCode) {
					return result && result.hasOwnProperty('css') ? result.secondCss : '';
				} else {
					return result && result.hasOwnProperty('css') ? result.css : '';
				}

			},
			Vue.prototype.codeToName = function(value, moduleCode) {
				if (!value) return ''
				let billStatusDictionary_ = billStatusDictionary;
				if (moduleCode) {
					billStatusDictionary_ = extendStatu[moduleCode];
				}
				let result = billStatusDictionary_.find((item) => {
					return item.value == value;
				})
				return result ? result.label : '';
			},
			// 获取状态数组集
			Vue.prototype.setExtendStatu = function(status, moduleCode) {
				extendStatu[moduleCode] = status;
			};
		//根据状态编码，获取状态name
		Vue.filter('codeToName', function(value, moduleCode) {
			if (!value) return ''
			let billStatusDictionary_ = billStatusDictionary;
			if (moduleCode) {
				billStatusDictionary_ = extendStatu[moduleCode];
			}
			let result = billStatusDictionary_.find((item) => {
				return item.value == value;
			})
			return result ? result.label : '';
		});
		//根据状态编码，获取状态颜色css
		Vue.filter('codeToBg', function(value, moduleCode) {
			if (!value) return ''
			let billStatusDictionary_ = billStatusDictionary;
			if (moduleCode) {
				billStatusDictionary_ = extendStatu[moduleCode];
			}
			let result = billStatusDictionary_.find((item) => {
				return item.value == value;
			})
			return result ? result.css : '';
		});
	}

}
