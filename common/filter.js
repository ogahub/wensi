import Vue from 'vue';
import config from '@/config';
// import utils from '@/utils';

Vue.filter('formatTime', (value, arg = 'yyyy/MM/dd')=>{
    if(isNaN(Number(value))){
        return value
    }
    return Comm.formatTime(value, arg)
})

// 状态转换文本
Vue.filter('statusToText', (value) => {
    let o = config.statusMap.find(item => value === item.value)
    return o ? o.label : ''
})

// 对账单类型转换文本
Vue.filter('statusToTextf', (value) => {
    let o = config.statusMapf.find(item => value === item.value)
    return o ? o.label : ''
})

// 工序类型转换文本
Vue.filter('statusToTextg', (value) => {
    let o = config.statusMapg.find(item => value === item.value)
    return o ? o.label : ''
})
// 收料
Vue.filter('statusToTextsl', (value) => {
    let o = config.statusMapsl.find(item => value === item.value)
    return o ? o.label : ''
})
//入库

Vue.filter('statusToTextrku', (value) => {
    let o = config.statusMapsrku.find(item => value === item.value)
    return o ? o.label : ''
})
//供应商

Vue.filter('statusToTextrsu', (value) => {
    let o = config.statusMapsrsu.find(item => value === item.value)
    return o ? o.label : ''
})

//颜色
 // Vue.filter('staColor', (value) => {
	// 		      switch (value) {
	// 		        case 'TOCONFIRM':
	// 		          return 'red';
	// 		        case 'CLOSE':
	// 		          return 'green'
	// 		      }
	// 		  })
