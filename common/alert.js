// import CONFIG from '../config'


class Alert {
	// toast提示
	toast(title, icon = "none") {
		uni.showToast({
			title: typeof title == 'string' ? title.substring(0, 100) : title + '',
			icon
		})
	}

	// loader显示
	showloading(title = "加载中") {
		uni.showLoading({
			title,
		});
	}

	// loader隐藏
	hideloading() {
		uni.hideLoading();
	}

	alert(msg, option = {}) {
		return new Promise((resolve, reject) => {
			uni.showModal({
				title: msg,
				content: '',
				// showCancel: false,
				confirmText: '确定',
				confirmColor: '#FF5858',
				...option,
				success: (result) => {
					console.log(555, result, option)
					if (result.confirm) {
						console.log(555, result, option)
						resolve(result.confirm)
					} else {
						reject(result.cancel)
					}
				},
				fail: (err) => {},
				complete: () => {}
			});
		})

	}

	confirm(msg, option = {}) {
		return new Promise((resolve, reject) => {
			uni.showModal({
				title: '提示信息',
				content: msg,
				showCancel: true,
				cancelText: '取消',
				cancelColor: '#d9493e',
				confirmText: '确定',
				confirmColor: '#0297f5',
				...option,
				success: (result) => {
					if (result.confirm) {
						resolve(result.confirm)
					} else {
						reject(result.cancel)
					}
				},
				fail: () => {},
				complete: () => {}
			});
		})

	}


}


export default new Alert()
