class Storage{
    constructor(cache_prefix){
        this.cache_prefix = cache_prefix || "cache_"
    }

    /**
     * @description: 设置储存
     * @param {String} 储存对应的key
     * @param {Any} 储存对应的val
     * @param {Boolean} 是否异步
     * @return: Promise
     */
    setData(key, data, async = false) {
        return new Promise((resolve,reject)=>{
            if (async) {
                uni.setStorage({
                    key,
                    data,
                    success: function () {
                        resolve();
                    },
                    fail: function(err){
                        console.error('保存失败')
                        reject(err);
                    }
                })
            } else {
                uni.setStorageSync(key, data)
                resolve();
            }
        })
    }

    // 获取储存
    getData(key) {
        return uni.getStorageSync(key)
    }

    // 删除储存
    removeData(key){
        uni.removeStorage({
            key,
            success: function(res) {},
            fail: function(err){
                console.error(err)
            }
        })
    }

    // 设置缓存
    setCache(key, data){
        return this.setData(this.cache_prefix + key, data)
    }

    // 获取缓存
    getCache(key){
        return this.getData(this.cache_prefix + key)
    }

    // 删除缓存
    removeCache(key){
        this.removeS(this.cache_prefix + key)
    }

    //将所有前缓存前缀的储存删除
    clearCache(){
        console.log('清除缓存clearCache')
        const keys = uni.getStorageInfoSync().keys;
        keys.forEach(key=>{
            if(key.indexOf(this.cache_prefix)!=-1){
				
                uni.removeStorageSync(key)
            }
        })
    }
}
export default new Storage();