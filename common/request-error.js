	
	export const  requestMsg = (response,i18nDatas) =>{
	    let message = i18nDatas.system_error;  //'系统内部错误';
		let statusCode = response.statusCode;
	     switch (statusCode) {
	    	      case 400:
	    	        return i18nDatas.wrong_request
	    	      case 401:
	    	        return i18nDatas.authentication
	    	      case 403:
	    	        return i18nDatas.server_rejects_reques
	    	      case 404:
	    	        return i18nDatas.non_server_fine_resource
	    	      case 405:
	    	        return i18nDatas.forbidden_request
	    	      case 406:
	    	        return i18nDatas.unable_respond
	    	      case 407:
	    	        return i18nDatas.proxy
	    	      case 408:
	    	        return i18nDatas.server_timed_out
	    	      case 409:
	    	        return i18nDatas.server_conflict
	    	      case 410:
	    	        return i18nDatas.requested_resource_deleted
	    	      case 411:
	    	        return i18nDatas.valid_content_not_accept
	    	      case 412:
	    	        return i18nDatas.precondition
	    	      case 413:
	    	        return i18nDatas.request_too_large;
	    	      case 414:
	    	        return i18nDatas.uri_too_long;
	    	      case 415:
	    	        return i18nDatas.unsupported_media;
	    	      case 429:
	    	        return  i18nDatas.operation_too_frequent //'您的操作过于频繁，请稍后重试！'
	    	      case 500:
	    	        message = i18nDatas.server_out;
	    	        if(response.data.code == 603){
	    	          return i18nDatas.service_not_available
	    	        }
	    	        if(response.data.code == 604){
	    	          return i18nDatas.information_not_maintained.replace('{0}', response.data.data)
	    	        }
	    	        if(response.data.code == 605){
	    	          return i18nDatas.cooperation_information_initiates
	    	        }        
	    	        if(response.data && response.data.message){
	    	           return response.data.message;
	    	        }
					console.log('message',message)
					console.log('response',response)
	    	        return message
	    	      case 501:
	    	        return i18nDatas.not_implemented
	    	      case 502:
	    	        return i18nDatas.bad_gateway
	    	      case 503:
	    	        return i18nDatas.service_not_available
	    	      case 504:
	    	        return i18nDatas.gateway_timeout
	    	      case 505:
	    	        return i18nDatas.http_version_not_supported
	    	      case 505:
	    	        return i18nDatas.http_version_not_supported   //'HTTP Version not supported (HTTP 版本不受支持)'         
	    	      default:
	    	        return message
	    	    }
	  };
	 