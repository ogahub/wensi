import {handleResult,getDictionary,formatListData,formatData,customValue,getSetDic,getCurrencySymbol,filterCurrencySymbol} from './main-service.js';
import {get,post,postwww,submit,show,upload} from './request';
import {OPEN_I18N,APPCONFIG} from '@/common/constants'; 
import store from '@/store';
export default {
  install (Vue, options) {
	Vue.prototype.$formatData = formatData;
	Vue.prototype.$customValue = customValue;
	Vue.prototype.$upload = upload;
	Vue.prototype.$show = show;
    Vue.prototype.$get = get;
    Vue.prototype.$post = post;
	Vue.prototype.$postwww = postwww;
	Vue.prototype.$submit =  submit;
	Vue.prototype.$store = store;
	Vue.prototype.$opVersion = APPCONFIG.opVersion;
	Vue.prototype.$getSetDic =  function(groupCodeaArr,code){
		return getSetDic(groupCodeaArr,code,submit,store);
	};
	Vue.prototype.$getCurrencySymbol =  function(){
		return getCurrencySymbol();
	};
	Vue.prototype.$filterCurrencySymbol =  function(code){
		return filterCurrencySymbol(code);
	};
	/**
	 * 处理服务器响应的接口数据
	 * @param {Object} data
	 * @param {Object} json
	 */
	Vue.prototype.$handleResult = handleResult;
	/**
	 * 处理服务器响应的接口数据
	 * @param {string} groupCode 
	 * @param {string} code
	 */
	Vue.prototype.$getDictionary =  function(groupCode,code){
		return getDictionary(groupCode,code,submit,store);
	}
	/*数据字典前置处理*/
	Vue.prototype.$initDict =  function(groupCodes){
		let promises = groupCodes.map(groupCode=>{
			return getDictionary(groupCode,[],submit,store);
		})
		return Promise.all(promises);
	}
	
	/*对api接口数据格式处理*/
	 Vue.prototype.$formatListData = function(data, templateData,targetKey,isFree=false){
		 return formatListData(data, templateData,post,store,targetKey,isFree);
	 }	
	
	 
	 /* 国际化变量替换 */
	 Vue.prototype.$pt = function() {
	     let i18nContent = arguments[0]; /* 国际化内容 */
	     if (arguments.length == 1) {
	         return i18nContent;
	     }
	     for(var i = 1; i < arguments.length; i++) {
	         i18nContent = i18nContent.replace(`{${i - 1}}`, arguments[i]);
	     }
	     return i18nContent;
	 }
	 /**
	  * 递归处理json中含有的国际化
	  * @param {Object} json
	  */
	 Vue.prototype.$configToI18n = function(json,prefix=''){
		 let hasLanguage = OPEN_I18N&&APPCONFIG.language!='zh_CN';
		 for(let key in json){
		 	let value = json[key];
		 	if(key.startsWith('$_')){
		 		let keys = key.split('$_');
				if(hasLanguage){
					/*转换处理*/
					json[keys[1]] = this.$t(`${prefix}${value}`);
				}
		 		delete json[key];
				/*冻结当前对象*/
		 	}else{
		 		if(value instanceof Object){
					/*冻结一项非必要配置属性*/
		 			this.$configToI18n(value);
		 		}
		 	}
		 }
	 }
	 /*冻结一项配置化的属性，优化性能 列表：LIST 详情：VIEW  编辑 EDIT*/
	 Vue.prototype.$freeze = function(json,type='LIST',freeField=[],freeJsonField=[]){
		if(type=='LIST'){
			 freeField = ['buttonGroup','list'];
			 freeJsonField = [{field:'fields',condition:{field:'type',reg:/state|date/}}];	 
		 }
		 freeField.push('i18nData');
		 /*冻结全对象*/
		 freeField.forEach(item=>{
			 if(json.hasOwnProperty(item)){
				 let tempItemValue = json[item];
				 if(Array.isArray(tempItemValue)){
					 tempItemValue.forEach(subItem=>{
					 Object.freeze(tempItemValue);
					 })
				 }else{
					 Object.freeze(tempItemValue);
				 }
			 }
		  })
		  /*冻结部分对象*/
		  freeJsonField.forEach(item=>{
			 let {field,condition} = item;
			 if(json.hasOwnProperty(field)){
				let tempObj = json[field];
				if(Array.isArray(tempObj)){
					tempObj.forEach(subItem=>{
						let {field,reg} = condition;
						if(subItem.hasOwnProperty(field)&&reg.test(subItem.type)){
							Object.freeze(subItem);
						}
					})
				}
			 }
		   })
	 	 }
  }
}
