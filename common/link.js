/*路由跳转处理*/
export const routeData = [{
	label: '我的待办',
	base: 'todo',
	value: 'todo',
	link: {
		LIST: "todo"
	}
}, {
	label: '采购订单',
	base: 'purchaseorder',
	value: 'CGD',
	link: {
		LIST: "purchase-order-list",
		VIEW: "purchase-order-detail"
	}
}, {
	label: '报价单',
	base: 'offer',
	value: ['XJD', 'BJD'],
	link: {
		LIST: "offer-list",
		VIEW: "offer-details"
	}
}, {
	label: '合同',
	base: 'contract',
	value: 'HT',
	link: {
		LIST: 'contract-list',
		VIEW: "contract-detail"
	},
}, {
	label: '发货单',
	base: 'delivery',
	value: 'ASN',
	link: {
		LIST: 'delivery-list',
		VIEW: "delivery-detail"
	}
}, {
	label: '招标单',
	base: 'tendering',
	value: ['BID', 'TEND', 'ABB', 'DEB', 'MPB', 'BSD', 'CON'],
	link: {
		LIST: 'tender-list',
		VIEW: 'tender-view'
	}
}, {
	label: '整改通知',
	base: 'reformnotice',
	value: ['ZGD','ZGH','ZGQ'],
	link: {
		LIST: 'reform-notice-list',
		VIEW: 'reform-notice-detail'
	}
}, {
	label: '竞价',
	base: 'auction',
	value: ['JJD','DBD'],
	link: {
		LIST: 'auction-list',
		VIEW: 'auction-view'
	}
}, {
	label: '物料认证',
	base: 'material',
	value: ['YPD'],
	link: {
		LIST: 'material-ypd-list',
		VIEW: 'material-ypd-detail'
	}
}, {
	label: '物料认证',
	base: 'material',
	value: ['SYD', 'SYDC'],
	link: {
		LIST: 'material-syd-list',
		VIEW: 'material-syd-detail'
	},
	}, {
		label: '实地评鉴',
		base: 'inspectresult',
		value: ['FEU','FEF'],
		link: {
			LIST: 'fieldevaluation-list',
			VIEW: 'fieldevaluation-detail'
		}
	}, {
		label: '绩效考核',
		base: 'inspectresult',
		value: ['VPN','PERFORMANCE'],
		link: {
			LIST: 'performance-list',
			VIEW: 'performance-detail'
		}
}]
