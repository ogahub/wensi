import {requestMsg} from './request-error.js';
import {APPCONFIG,I18N_DATAS} from '@/common/constants';
import {hasExisteWhite} from '@/common/common' 
let headers = {
	'Access-Control-Allow-Origin': '*',
	'Accept': 'application/json',
	'clientId': 'app'
};
let mockBaseUrl = '';//http://192.168.0.101:7002
let mockApi = `${mockBaseUrl}/mock`;
function request(url, method, data,loader,appendHeader={}) {
	 let headers_ = JSON.parse(JSON.stringify(headers));
	 Object.assign(headers_,appendHeader);
	let {token,baseUrl,language} = APPCONFIG;
	headers_['language'] = language;
	if(!hasExisteWhite(url)){
		headers_['Authorization'] = token;
	}
    url = url.startsWith('/') ? `${mockApi}${url}` : `${baseUrl}${url}`;
	if(loader){
		uni.showLoading({
			title:   "" //'请求中...'
		    // title:   I18N_DATAS.on_request //'请求中...'
		});
	}
	return uni.request({
		url,
		method,
		data,
		dataType: 'json',
		header: headers_
	}).then(data => {
		if(loader){
			uni.hideLoading();
		}
		let result;
		let [error, res] = data;
		//console.log('error==>',data)
		if (error) {
			return Promise.reject(error)
		} else if (res.statusCode == 200) {
			if (res.hasOwnProperty('data')) {
				result = res.data;
				result = result==="[]"?"":result;
				// console.log(333,result,result==="'[]'")
				if (result.hasOwnProperty('data')) {
					let data = result.data;
					// console.log(3334,data,data==="[]")
					data = data==="[]"?"":data;
					if (typeof data == 'string') {
						 result.data = data.indexOf('{')==-1?data:JSON.parse(data);
					}
				}
				/*错误信息提示*/
				if (result.hasOwnProperty('success') && !result.success && result.hasOwnProperty('errorMsg')) {
					uni.showToast({
						title: result.errorMsg,
						icon: 'none'
					})
				}
			} else {
			}
			return result;
		} else {
			return Promise.reject(res);
		}
	}).catch(err => {
		if(loader){
			uni.hideLoading();
		}
		switch (err.statusCode) {
			case 401:
				uni.clearStorageSync()
				break
			default:
				let errorMsg = requestMsg(err,I18N_DATAS);
				show(errorMsg);
				return Promise.reject()
				break
		}
	})
}

export const get = (url, data,loader=false) => {
	return request(url, 'GET', data,loader);
}
export const submit = (url, data,loader=false) => {
	return request(url, 'POST', initParams(data),loader);
}
export const post = (url, data,loader=false) => {
	return request(url, 'POST',initParams(data),
	loader,{"Content-Type":"application/json"},
	);
}

export const postwww = (url, data,loader=false) => {
	return request(url, 'POST',initParams(data),
	loader,{"Content-Type":"application/x-www-form-urlencoded;charset=utf-8"},
	);
}
/**
 * @param {Object} data 初始化参数
 */
function initParams(data){
		if(data&&data.hasOwnProperty('pageNo')){
			let copyData = {...data}
			// copyData['start'] = copyData.pageNo*data.pageSize;
			copyData['page'] = copyData.pageNo+1;
			copyData['pageSize'] = copyData.pageSize;
			delete copyData.pageNo;
			// delete copyData.pageSize;
			return copyData;
		}else{
			return data;
		}
	}
/**
 *
 * @param {*} url   请求的url
 * @param {*} data  请求参数
 * @param {*} fileGroupId  附件上传的附件组
 * @param {*} storageType  存储类型
 */
export const upload = async (files, fileGroupId='', storageType = 'srm',url="file/upload") => {
	files = files.filter(item=>{
		return !item.fileCode;
	})
	if(files.length<=0){
		return Promise.resolve({fileGroupId:fileGroupId})
	}
	//三种上传方式
	let data = [];
	let localFile = [];
	let uniappFile = [];
	files.forEach(item=>{
		if(item.hasOwnProperty('attrInfo')){
			data.push(item);
		}else if(item.hasOwnProperty('path')){
			uniappFile.push(item);
		}else{
			localFile.push(item);
		}
	})
	let {token,baseUrl} = APPCONFIG;
	let result;
	let files_p = [];
	if(data.length>0){
	result = await  uploadFile(url,data,fileGroupId,storageType);
	}
	if(uniappFile.length>0){
	 result = await  uploadFileByUniapp(url,uniappFile[0],result?result.data.fileGroupId:false,storageType);	
	
	 for(let i=1;i<uniappFile.length;i++){
		result = await  uploadFileByUniapp(url,uniappFile[i],result.data.fileGroupId,storageType);
	 }
	}
	return new Promise((resolve,reject)=>{
		if(localFile.length>0){
		let time = new Date().getTime();
		window.uploadFile({attrs:localFile,fileGroupId:(result?result.data.fileGroupId:fileGroupId),storageType:storageType},(successful, result)=>{
		  let callBackFun = `uploadFile_${time}`
		  delete window[callBackFun];
		  if(successful){
			resolve({fileGroupId:result})
		  }else{
			reject({fileGroupId:result});
		  }
		},time)
		}else{
			resolve(result?{fileGroupId:result.data.fileGroupId}:{fileGroupId:fileGroupId});
		}
	})
}; 
/**
 * 基于uniapp上传
 */
function uploadFileByUniapp(uploadUrl="file/upload", fileList, fileGroupId=false, storageType = 'srm'){
	// console.log('fileGroupId',fileGroupId)
	let {token,baseUrl} = APPCONFIG;
	return new Promise((reslove,reject)=>{
		let formData = { };
		if(fileGroupId){
		   formData['fileGroupId'] = fileGroupId;
		}
		let uploadHeader = {'clientId':'pc','storageType':storageType};
		if (token) {
		    // 让每个请求携带token
		    uploadHeader['Authorization'] = token;
		  }
		   uni.uploadFile({
				  url: `${baseUrl}${uploadUrl}`, //仅为示例，非真实的接口地址
				  filePath: fileList.path,
				  name: 'file',
				  header:uploadHeader,
				  formData: formData,
				  success: (uploadFileRes) => {
					  let result =JSON.parse(uploadFileRes.data);
					  //console.log('result',result);
					  if(result.code !=='0000'){
						  result.data.fileGroupId = fileGroupId;
					  }else{
						 	// uni.showToast({
						 	// 	title: result.errorMsg,
						 	// 	icon: 'none',
						 	// 	duration: 2000

						 	// })
					  }
					 reslove(result);
				  }
			  });
	})
}
/**
 * 基于form文件上传
 */
function uploadFile(url="file/upload", data, fileGroupId=false, storageType = 'srm'){
	let {token,baseUrl} = APPCONFIG;
	return new Promise((resolve, reject) => {
	    let isArr = Array.isArray(data);
	    if (!isArr || (isArr && data.length == 0)) {
	        let err = I18N_DATAS.upload_file_abnormal;   //'上传文件格式异常或为空!';
	        reject(err);
	        throw new Error(err);
	        return false;
	    }
	    var upData = new FormData();
	    let upFiles = [];
	        url= `${baseUrl}${url}`
	        data.forEach(function (file) {// 因为要上传多个文件，所以需要遍历
	            if (!file.hasOwnProperty('fileCode')) {
	                upData.append('file', file.attrInfo, file.name);
	                upFiles.push(file.name);
	            }
	        })
	        if(fileGroupId){
	            upData.append('fileGroupId', fileGroupId + '');
	        }
	    if (upFiles.length == 0) {
	        let err = I18N_DATAS.no_files_upload ;//'暂无需要上传的文件!';
	        reject(err);
	        throw new Error(err);
	        return false;
	    }
	    var xhr = new XMLHttpRequest();
	    xhr.open('POST', url, true);
	    if (token) {
	        xhr.setRequestHeader('Authorization',token);
	      }
	    xhr.setRequestHeader('clientId','pc');
	    xhr.setRequestHeader('storageType', storageType);
	    xhr.onreadystatechange =  (res)=> {
	        if (xhr.readyState == 4 && xhr.status == 200) {
	          var resp = JSON.parse(xhr.responseText);
	          let isError = (resp.hasOwnProperty('error') && resp.error) || (resp.hasOwnProperty('success') && !resp.success);
	          let error = resp.hasOwnProperty('error') ? resp.error : (resp.hasOwnProperty('success') ? resp.message : null );
	          if (isError) {
	            reject(error);
	            throw new Error(error);
	          } else {
	            let fileData;
	            if (resp.data) {
	                fileData = resp.data;
	            } else if (resp.extra) {
	                fileData = resp.extra;
	            }
	            resolve(resp);
	          }
	        }
	      };
	      xhr.onerror =  (error) =>{
	        reject(error);
	      };
	      xhr.send(upData);
	})
}
/**
 * 显示消息
 */
export const show = (message = I18N_DATAS.operation_completed,icon='none', duration = 1500) => {
	uni.showToast({
		title: message,
		icon: icon,
		duration: duration
	})
}
