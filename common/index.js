/**
 * 注入 vue 原型
 */
import filter from './filter.js'



import { HttpInstance } from '../api/base/http.js'

import { debounce } from './utils.js'
import validate from './validate'

import Alert from './alert'
import Storage from './storage'

export default {
	onReady() {
		// let capsule = wx.getMenuButtonBoundingClientRect();
		console.log('capsule',capsule)
	},
	
	install(Vue, options) {
		
		Vue.prototype.$http = HttpInstance

		Vue.prototype.$validate = validate
		
		Vue.prototype.$debounce = debounce
		Vue.prototype.$alert = Alert
		Vue.prototype.$storage = Storage
	}
}
