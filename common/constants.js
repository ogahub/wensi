/**
 * 公共的国际化资源文件
 */
export const I18N_DATAS = {
	system_error: 'system_error', //系统那边错误
	operation_completed: 'operation_completed', //操作完成
	exporting: 'exporting', //导出中
	on_request: 'on_request', //请求中
	no_files_upload: 'no_files_upload', //暂无需要上传的文件
	upload_file_abnormal: 'upload_file_abnormal', //上传文件格式异常或为空
	wrong_request: "wrong_request",
	authentication: "authentication",
	server_rejects_reques: "server_rejects_reques",
	non_server_fine_resource: "non_server_fine_resource",
	forbidden_request: "forbidden_request",
	unable_respond: "unable_respond",
	proxy: "proxy",
	server_timed_out: "server_timed_out",
	server_conflict: "server_conflict",
	requested_resource_deleted: "requested_resource_deleted",
	valid_content_not_accept: "valid_content_not_accept",
	precondition: "precondition",
	request_too_large: "request_too_large",
	uri_too_long: "uri_too_long",
	unsupported_media: "unsupported_media",
	not_implemented: "not_implemented",
	bad_gateway: "bad_gateway",
	service_not_available: "service_not_available",
	gateway_timeout: "gateway_timeout",
	http_version_not_supported: "http_version_not_supported",
	server_out: 'server_out', //服务器开小差了，请您稍后再试！,
	service_not_available: 'service_not_available', //业务中台服务不可用，请您稍后再试！',
	information_not_maintained: 'information_not_maintained', //'您在{0}下的供应商信息暂未维护，请前往企业信息-合作申请维护供应商信息！',
	cooperation_information_initiates: 'cooperation_information_initiates', //'您在企业信息-合作申请中暂未维护合作信息，请前往企业信息-合作申请发起合作申请！',
	operation_too_frequent: "operation_too_frequent", // "您的操作过于频繁，请稍后重试"
	http_version_not_supported: "http_version_not_supported" //"HTTP 版本不受支持"
}
export const PAGE_SIZE = 5; //默认分页大小
/*选择器分页数据*/
export const COMMON_PICKER_PAGE_SIZE = 15;

export const DATA_DICTIONARY = "masterdata/finddatadictall";
/*获取客户接口*/
export const FIND_CUSTOMER = "findmycustomer";

/* 附件下载 */
export const FILE_DOWNLOAD_URL = "file/download";

/* 附件组列表 */
export const FILE_GROUP_LIST = "console/fs/file/getall";

/** 附件上传 */
export const FILE_UPLOAD_URL = 'file/upload';

export const TOKEN_KEY = "webConfig"; //供应商令牌信息
/* 是否开启国际化 */
export const OPEN_I18N = true;
/* 本地I18N前缀 */
export const I18N_PREFIX = 'qscLocal';
/**
 * 测试账户列表
 * enable：启用或者禁用enable列表
 */
import Token from './token.js'
import config from '../config.js';

let testAccount = [
	/*测试1*/
	{
		token: "", //baseUrl:"http://192.168.1.93:8802/qsc-JOKER/",
		customerCorpNo: '',
		baseUrl: "http://192.168.1.93:8802/qsc-app/app/",
		/*登录url*/
		devUrl: '',
		/*用来指定某个开发机的ip*/
		language: 'en',
		opVersion: false,
		enable: false,
	},
	/**测试账户2 http://10.10.20.144:8802/qsc/   http://10.10.10.203:31501/qsc-app/app/**/
	{
		token: Token.getToken(),
		customerCorpNo: '',
		// baseUrl:"http://10.10.20.24:9981/app/",
		// baseUrl: "http://10.10.21.35/gateway/",
		
		baseUrl: config.baseUrl+'/gateway/',
		language: 'zh_CN',
		opVersion: true,
		enable: true
	}
]
/**
 * 存放全局的客户端信息
 */
export const APPCONFIG = testAccount.find(item => {
	return item.enable
});
