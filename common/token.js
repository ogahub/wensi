import Storage from './storage'

class Token {
    constructor(){
        this.default_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjYWNoZUtleSI6IkNPTlNPTEU6VVNFUl9UT0tFTjoxNDUyNDE5MDI4NTIxMTE5NzQ0IiwiY2xpZW50Q29kZSI6IjgwMCIsImlzcyI6Ik1JTkciLCJpc0FwcCI6ZmFsc2UsInVzZXJDb2RlIjoiUDAwMDEwMzMifQ.N0yI04maNAzmUmpUMktEBUvqlXgB-cHhppztAnDyBHM'
        // this.default_token = ''
    }

    getToken(){
        return Storage.getData('qzing_token') || this.default_token
    }

    setToken(token){
        Storage.setData('qzing_token', token, true)
    }

    //备份token用于登出后的注册接口token需要
    getTokenCopy(){
        return Storage.getData('token_copy')
    }

    setTokenCopy(token){
        Storage.setData('token_copy', token, true)
    }
}

export default new Token()