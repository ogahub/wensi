
/**
 * @param {Object} params
 */
function openFile(params) {
  if (window.webkit) {
    webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({
      method: "openFile",
      args: [null, params]
    }))
  }
}
/**
 * @param {Object} params
 */
function viewDoc(params) {
  if (window.webkit) {
    webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({
      method: "viewDoc",
      args: [null, params]
    }))
  }
}
/**
 * @param {Object} params
 */
function getCustomerCorpNo(params) {
  if (window.webkit) {
    webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({
      method: "getCustomerCorpNo",
      args: [null, params]
    }))
  }
}
/**
 * 获取图片base64
 * @param {*} params 
 * @param {*} callbackName_ 
 */
function getFileBase64(params, callbackName_) {
  window['getFileBase64_'] = callbackName_;
  webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({
    method: "getFileBase64",
    args: ['getFileBase64_', params]
  }))
}
/**
 * @param {Object} params
 * @param {Object} callbackName_
 */
function getLocalPath(params,callbackName_){
	window['getLocalPath_'] = callbackName_;
	 if (window.webkit) {
		webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({
		  method: "getLocalPath",
		  args: ['getLocalPath_', params]
		})) 
	 }else{
		 uni.hideLoading();
	 }
	
}

/**拍照上传图片
 * @param {Object} params
 * @param {Object} callbackName_
 */
function camera(params, callbackName_) {
  if (window.webkit) {
    window['camera_'] = callbackName_;
    webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({
      method: "camera",
      args: ['camera_', params]
    }))
  }
}
/**
 * @param {Object} params
 * @param {Object} callbackName_
 * 扫码处理
 */
function scan(params, callbackName_) {
  if (window.webkit) {
    window['scan_'] = callbackName_;
    webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({
      method: "scan",
      args: ['scan_', params]
    }))
  }
}
/**
 * @param {Object} params
 * @param {Object} callbackName_
 * @param {Object} time
 * 附件上传
 */
function uploadFile(params, callbackName_,time) {
  if (window.webkit) {
    var callBack = 'uploadFile_'+time; 
    window[callBack] = callbackName_;
    webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({
      method: "uploadFile",
      args: [callBack, params]
    }))
  }
}
/**
 * @param {Object} params
 * 关闭内置浏览器
 */
function closeInBrowser(params) {
  if (window.webkit) {
    webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({
      method: "closeInBrowser",
      args: [null, params]
    }))
  }
}

/**获取网络状态
 * @param {Object} callbackName_
 */
function getNetworkType(callbackName_){
  if (window.webkit) {
    var item = 'getNetworkType';
    var itemKey = item + '_';
    window[itemKey] = callbackName_;
    webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({
      method: item,
      args: [itemKey, {}]
    }))
  }
}
/**
 * 导出设备交互方法，给外部组件使用
 */
module.exports = {
	openFile,
	viewDoc,
	getCustomerCorpNo,
	getFileBase64,
	getLocalPath,
	camera,
	scan,
	uploadFile,
	closeInBrowser,
	getNetworkType
}