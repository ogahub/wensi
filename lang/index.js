import {APPCONFIG} from '@/common/constants'; 
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import enLocale from './en'
import zhLocale from './zh'
Vue.use(VueI18n)
/**发起请求获取后端国际化资源文件 */
const messages = {
  en: {
    ...enLocale,
  },
  zh_CN: {
    ...zhLocale,
  }
}
const i18n = new VueI18n({
  locale: APPCONFIG.language,
  messages
})
export default i18n
