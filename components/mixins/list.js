import {
	mapState
} from 'vuex'
import {
	debounce
} from '@/common/utils.js';
import {
	APPCONFIG
} from '@/common/constants.js';
export default {
	provide() {
		return {
			receiveParams: (searchParams, flag, type) => {
				if (flag) {
					//this.$refs.loadRefresh.goTop();
					Object.assign(this.params, searchParams);
					this.$refs.loadRefresh.goTop();
					//查询
					this.$nextTick(function() {
						if (type && type == 'reset') {
							this.search.show = true;
						} else {
							this.search.show = false;
						}
						this.loadData(false, true);
					})
				} else {
					this.$nextTick(function() {
						this.search.show = false;
					})
				}
			}
		}
	},
	data() {
		return {
			fcShow: false,
			fcParams: {}
		}
	},
	onLoad() {
		/*判断是否有初始化全局数据字典的*/
		/*tabr处理*/
		let bar = this.tabs.find(item => {
			return item.type == 'bar'
		})
		if (bar) {
			let curBar = bar.list.find((item, index) => {
				let checkIndex = item.hasOwnProperty('checkIndex') ? item.checkIndex : 0;
				return index == checkIndex;
			})
			Object.assign(this.params, curBar ? curBar.params : {});
		}
		/*是否有配置前置处理*/
		let promises = [];
		if (this['beforeHandleConfig']) {
			promises.push(this.beforeHandleConfig())
		}
		Promise.all(promises).then(res => {
			let hasDict = false;
			this.search.config.fields.forEach((item, index) => {
				if (item.hasOwnProperty('dictKey')) {
					this.$getDictionary(item.dictKey, []).then(result => {
						//console.log('==1==',result);
						if (Array.isArray(result)) {
							item[item.key].data = result.map(subItem => {
								let {
									itemCode,
									itemName
								} = subItem;
								return {
									title: itemName,
									code: itemCode,
									isSel: false
								}
							})
							this.$set(this.search.config.fields, index, item);
							if (this.$refs.customeSearchForm) {
								this.$refs.customeSearchForm.hasDict = true;
							}
						}
					}, err => {
						this.$refs.customeSearchForm.hasDict = true;
					})
				}
			});
			if (this.search.config.fields.every(item => {
					return !item.hasOwnProperty('dictKey')
				})) {
				this.$nextTick(() => {
					// console.log('customeSearchForm==', this.$refs.customeSearchForm.hasDict);
					if (this.$refs.customeSearchForm) {
						this.$refs.customeSearchForm.hasDict = true;
					}
				})
			}
		}, err => {
			this.$show('beforeHandleConfig 方法执行失败')
		})


	},
	onShow: function(e) {
		if (this.isDoRefresh) {
			this.loadData(false, true);
			this.$store.commit('setDoRefresh', false);
		}
		// 临时处理小数位处理0427，后面改到登录时请求
		if(localStorage.getItem('unit_price_decimal')==null){
			// 获取数量、价格小数位接口缓存在本地
			// this.getNumberPriceDecimal();
		}
		
		
		
	},
	computed: {
		isDoRefresh() {
			return this.$store.state.isDoRefresh;
		}
	},
	methods: {
		// 获取数量、价格小数位接口缓存在本地
		getNumberPriceDecimal() {
			// 数量、价格
			const request = ['masterdata/findunitall', 'masterdata/findcurrencyall','masterdata/finduserstrategy'];
			const promiseArr = request.map(it => {
				return new Promise((resolve, reject) => {
					this.$post(it, {}).then(
						res => {
							resolve(res.data);
						},
						err => {
							console.info('请求异常', error);
						}
					);
				});
			});
			Promise.all(promiseArr)
				.then(result => {
					let obj = {
						unit_decimal: result[0],
						price_decimal: result[1],
						currency_round: result[2]
					};
					// console.info('全部请求完=====', obj);
					localStorage.setItem('unit_price_decimal',JSON.stringify(obj))
				})
				.catch(error => {
					console.info('请求异常', error);
				});
		},
		bindPickerChange(e) {
			
			this.index = e.target.value;
			this.customerName = this.arrayfindmycustomerList[this.index];

			let currentCustomer = this.findmycustomerList.find(item => {
				return this.customerName == item.customerName
			})
			if (currentCustomer) {
				this.customerCorpNo = currentCustomer.customerCorpNo
			}
			this.refreshcustomerList(this.customerCorpNo);
			/*存储客户编码*/
			try {
			    uni.setStorageSync('customerCorpNo', this.customerCorpNo);
			} catch (e) {
			    // error
			}
		},
		// 获取企业信息列表
		getFindmycustomerList() {
			let url = 'findmycustomer',
				type = 'post';
			this[`$${type}`](url, this.params).then(res => {
				let customerCorpNo = APPCONFIG.customerCorpNo || uni.getStorageSync("customerCorpNo");
				this.findmycustomerList = res.data.map(item => {
					let {
						customerName,
						customerCorpNo
					} = item;
					return {
						customerName,
						customerCorpNo
					}
				});
				this.arrayfindmycustomerList = this.findmycustomerList.map(item => {
					return item.customerName;
				})
				let currentCustomer = this.findmycustomerList.find((item, index) => {
					let tempFlag = (customerCorpNo == item.customerCorpNo);
					if (tempFlag) {
						this.index = index;
					}
					return tempFlag;
				})
				if (currentCustomer) {
					this.customerName = currentCustomer.customerName;
				}
			})
		},
		// 切换合作客户刷新企业中台配置信息
		refreshcustomerList(customerCorpNo, flag = true) {
			let url = 'refreshcustomer',
				type = 'post';
			this[`$${type}`](url, {
				customerCorpNo: customerCorpNo
			}).then(res => {
				if (res.success) {
					this.getData(false, true);
					if (flag) {
						window.getCustomerCorpNo({
							customerCorpNo
						})
					}
				} else {
					this.$show(res.message)
				}

			})
		},
		chooseTab(data) {
			console.log('data',data)
			let self = this;
			let {
				item
			} = data;
			if (item.hasOwnProperty('params')) {
				self.data = [];
				// setTimeout(() => {
					self.params = Object.assign(self.params, item.params);
					if(item.hasOwnProperty('billType')) {
						self.getData(false,true,item.billType);
					}else{
						self.getData(false,true)
					}
					
				// }, 500)
			}
		},
		initSearchParams(flag) {
			if (flag) {
				this.params.pageNo++;
			} else {
				this.data = [];
				this.params.pageNo = 0;
			}
		},
		/**
		 * 上拉加载
		 */
		loadMore: debounce(function() {
			this.loadData(true);
		}),
		/**
		 * 下拉刷新数据列表
		 */
		refresh() {
			this.getData(false);
		}
	},
	onUnload() {
		this.getData = null;
		this.loadMore = null;
	}
}
