export default {
    data() {
        return {
		   hasTipBar:false,
           CustomBar:this.CustomBar,
           scrollInto:'',
		   fcShow:false,
		   fcParams:{
		   }
        }
    },
    
    computed: {
      getTabName(code){
        return (code)=>{
      	  let tab = this.tabMenus.find(item=>{
      		return item.code==code
      	  })
      	return tab?tab.name:''
        }
      },
	  scrollTop(){
		  return uni.upx2px(this.hasTipBar?(96+75):96);
	  },
	  contentHeight(){
	  		  return this.screenHeight-this.scrollTop-this.CustomBar;
	  }
     },
	
    methods: {
      chooseTab(data){
         this.scrollInto = data.item.code;
      },
      actionOpen(e){
		  let code = e.currentTarget.dataset.code;
		  let index = e.currentTarget.dataset.index;
		  let dtls = this.billInfo[code];
		  if(dtls){
			  let dtl = dtls[index];
			  if(!dtl.hasOwnProperty('isOpen')){
				  this.$set(this.billInfo[code][index],'isOpen',false);
			  }else{
				  dtl.isOpen = !dtl.isOpen;
			  }
		  }
      },
	  /**
	   * @param {Object} params
	   * 导出单据
	   */
	  exportBillInfo(params){
		  /*r如果是android*/
		  uni.showLoading({title: this.$t('qscLocal.common.exporting')});//'导出中...'
		  if(typeof(params.fileName)=='string'){
			  if(params.fileName.indexOf('_')==-1){
				  params.fileName = params.fileName+'_'+this.fileName;
			  }
		  }
		  window.getLocalPath(params,(successful, result)=>{
		    if(successful){
				if(this.platform=='ios'){
				window.viewDoc(params);
				}else{
				window.openFile(params);
				 // window.open(res.data)
				}
			   uni.hideLoading();
			  delete window['getLocalPath'];
		    }else{
		  	 uni.hideLoading();
		  	 delete window['getLocalPath'];
		    }
	  })
    }
	}
}