import {
	autoFillZero
} from '@/common/utils.js'
import {
	mapGetters
} from 'vuex';
export default {
	provide() {
		return {
			setForm: (parentNodeName, index, nodeValue) => {
				if (index != -1) {
					this.$set(this.forms[parentNodeName], index, nodeValue);
				} else {
					this.$set(this.forms, parentNodeName, nodeValue);
				}
			},
			setFormItem: (parentNodeName, index, nodeName, nodeValue, hasEvent) => {
				if (index != -1) {
					this.forms[parentNodeName][index][nodeName] = nodeValue;
				} else {
					this.forms[parentNodeName][nodeName] = nodeValue;
				}
				if (hasEvent) {
					setTimeout(() => {
						this.setFormItem(parentNodeName, index, nodeName, nodeValue);
					})
				}
			},
			changeSelectData: (data) => {
				this.selectData(data);
			}
		}
	},
	data() {
		return {
			CustomBar: this.CustomBar,
			errorMsg: {},
			scrollInto: '',
			open: false,
			keyHeight: 0,
			tabBarHeight: uni.upx2px(146),
			tabPos: {}
		}
	},
	onLoad() {},
	computed: {
		// keyboradInfo() {
		// 	return this.$store.state.keyboradInfo;
		// },
		getTabName(code) {
			return (code) => {
				let tab = this.tabMenus.find(item => {
					return item.code == code
				})
				return tab ? tab.name : ''
			}
		},
		contentHeightForm() {
			if(this.platform =='ios'){
				return this.screenHeight - this.CustomBar - this.keyboardHeight + (this.keyboardHeight > 10 ? this.btnHeight : 0);
			}else{
				return this.screenHeight - this.CustomBar - this.keyboardHeight;
			}
			
		},
		warpPaddingB() {
			return this.platform !== 'ios' && this.keyboardHeight > 10 ? this.keyboardHeight-this.btnHeight : 0;
		},
		...mapGetters({
			keyboardHeight: 'keyboardHeight',
			showModal: 'showModal'
		})
	},
	methods: {
		/**
		 * @param {Object} dataBaseInfo  forms数据
		 * @param {Object} configBaseInfo  forms json配置
		 * @param {Object} formName  forms 对象命名
		 */
		afterHandleData(dataBaseInfo, configBaseInfo,formName) {
			let result = configBaseInfo.fields.filter(item => {
				return item.labelName
			});

			let promiseArr = [];
			result.forEach(res => {
				let promiseItem = new Promise((resolve, reject) => {
					let {
						value,
						labelName
					} = res;
					let {
						params,
						toItem
					} = res[value];
					let {
						dictKey,
						url,
						requestType
					} = params;
					let serachParams = {};
					serachParams[value] = dataBaseInfo[value];
					// dictKey,排除数据字典的
					if (dictKey) {
						this.$getDictionary(dictKey, []).then(result => {
							if (Array.isArray(dataBaseInfo)) {
								dataBaseInfo.forEach((item, index) => {
									let result_ = result.find(item => {
										return item.itemCode == dataBaseInfo[index][value];
									})
									if (result_) {
										dataBaseInfo[index][labelName] = result_.itemName;
										resolve(labelName);
									}
								})
							} else {
								let result_ = result.find(item => {
									return item.itemCode == dataBaseInfo[value];
								})
								if (result_) {
									dataBaseInfo[labelName] = result_.itemName;
									resolve(labelName);
								}
							}
						})
					} else {
						this[requestType](url, serachParams).then(res => {
							var result = this.$handleResult(res, toItem);
							if (result && Array.isArray(result)) {
								let result_ = result.find(item => {
									return item.itemCode == dataBaseInfo[value];
								})
								if (result_) {
									dataBaseInfo[labelName] = result_.itemName;
									resolve(labelName);
								}
							}
						});
					}
				})
				promiseArr.push(promiseItem);
			});

			Promise.all(promiseArr).then(pro_res => {
				//console.info('pro_res==', pro_res,dataBaseInfo,formName);
				//所有主数据请求过滤完再重新渲染form表单
				this.$set(this.forms,formName, dataBaseInfo);
			}).catch(err=>{
				console.info(err)
			})
		},
		// chooseTab(data) {
		// 	this.scrollInto = data.item.code;
		// },
		chooseTab(data) {
			let code = '';
			let temp = 0;
			if ((typeof(data) == 'string')) {
				code = data;
				temp = 30;
			} else {
				code = data.item.code;
			}
		if (!this.tabPos.hasOwnProperty(`${code}`)) {
				uni.createSelectorQuery().select("#wrap").boundingClientRect(data=>{//目标节点
				　　uni.createSelectorQuery().select(`#${code}`).boundingClientRect((res)=>{//最外层盒子节点
				　　　//console.log('res.top - data.top',res.top - data.top)50　;
				     //alert(res.top - data.top)
				     uni.pageScrollTo({
				　　　　　　duration:0,//过渡时间必须为0，uniapp bug，否则运行到手机会报错
				　　　　　　scrollTop:res.top - data.top-temp,//滚动到实际距离是元素距离顶部的距离减去最外层盒子的滚动距离
				　　　　})
				　　}).exec()
				}).exec();
		
		} else {
			uni.pageScrollTo({
				duration: 10, //过渡时间必须为0，uniapp bug，否则运行到手机会报错
				scrollTop: this.tabPos[code], //滚动到实际距离是元素距离顶部的距离减去最外层盒子的滚动距离
			})
		}
		},
		/**
		 * 处理表单数据
		 * @param {Object} data
		 * @param {Object} templateData
		 */
		handleFormData(data, templateData, code = '') {
			// console.log(data,templateData);
			let newFileds = new Set();
			let {
				fields,
				uploadFile,
				others
			} = templateData;
			if (others) {
				others.forEach(item => {
					newFileds.add(item);
				})
			}
			if (uploadFile) {
				uploadFile.forEach(item => {
					newFileds.add(item.value);
				})
			}
			if (fields) {
				fields.forEach(item => {
					let {
						type,
						value
					} = item;
					if (item.hasOwnProperty('toMapper')) {
						//console.log('item',item)
						let {
							itemName,
							itemCode
						} = item.toMapper;
						newFileds.add(itemName);
						newFileds.add(itemCode);
					} else {
						newFileds.add(item.value);
					}

				})
			}
			if (Array.isArray(data)) {
				let result = data.map((item, index) => {
					return this.handleFormItem(item, newFileds, fields, code, index);
				})
				return result;
			} else {
				return this.handleFormItem(data, newFileds, fields, code, -1);
			}
		},
		/**
		 * 处理表单每一项
		 * @param {Object} item
		 */
		handleFormItem(item, templateData, fields = [], code = '', index = -1) {
			let itemData = {};
			let self = this;
			if (templateData) {
				templateData.forEach(formItem => {
					if (formItem.indexOf('>') != -1) {
						let otherKeys = formItem.split('>');
						let [key, dict, newField] = otherKeys;
						let oterValue = item[key] || '';
						if (oterValue) {
							self.$getDictionary(dict, oterValue).then(res => {
								itemData[newField] = res;
							})
						} else {
							itemData[key] = oterValue;
						}
					} else if (formItem.indexOf(".") != -1) {
						let sub_pro = formItem.split(".");
						if (sub_pro.length >= 2) {
							if (!itemData.hasOwnProperty(sub_pro[0])) {
								itemData[sub_pro[0]] = {};
							}
							itemData[sub_pro[0]][sub_pro[1]] = item[sub_pro[0]][sub_pro[1]] || '';
						}
					} else {
						let result = item[formItem] || '';
						/*格式化处理数据*/
						let field = fields.find(item => {
							return item.value == formItem;
						})
						if (field) {
							let {type} = field;
							if (result) {
								if (type == 'digit' && field.hasOwnProperty('num')) {
									result = autoFillZero(result, field.num);
								} else if (type == 'date' && field.hasOwnProperty('format')) {
									result = this.$formatData(result, field.format);
								}else if(type == 'number'){
									result = item[formItem]
								}
								/*如果field是select需要特殊处理*/
								if (type == 'select' && code) {
									
									/*是否来源于数据字典*/
									let selectParams = field[formItem].params;
									if (selectParams.dictKey) {
										self.$getDictionary(selectParams.dictKey, []).then(dicts => {
											let temp_dict = dicts.find(item => {
												return item.itemCode == result;
											})
											if (temp_dict) {
												self.setInitFiled(code, index, field, temp_dict);
											}
										})
									} else if (formItem != 'taxRateValue') {
										//非数据字典来源处理
										//console.log('field',field)
										let {
											url,
											requestType
										} = field[formItem].params;
										let toItem_ = field[formItem].toItem;
										let requestType_ = requestType ? requestType : '$submit';
										let searchParams = {};
										searchParams[formItem] = result;
										self[requestType_](url, searchParams).then(res => {
							
											var result = self.$handleResult(res, toItem_);
											// console.log(searchParams,res.data,toItem_,result);
											// console.log(code,index,field,result[0]);
											if (result && result.length > 0) {
												self.setInitFiled(code, index, field, result[0]);
											}
										})
									}
								}
								itemData[formItem] = result;
							}else{
								if(type == 'number'){
									itemData[formItem] = item[formItem]
								}
							}
						}else{
							itemData[formItem] = result;
						}
					}
				})
			}
			return itemData;
		},
		setInitFiled(parentNodeName, index, item, result) {
			// console.log(parentNodeName,index,item,result);
			let curForms = index == -1 ? this.forms[parentNodeName] : this.forms[parentNodeName][index];
			let {
				toMapper
			} = item;
			if (toMapper) {
				let keys = Object.keys(toMapper)
				keys.forEach(key => {
					let value = toMapper[key];
					curForms[value] = result[key];
				})
			} else {
				curForms[name] = result.itemCode;
			}
		},
		/**
		 * 校验表单
		 * @param {Object} formKey
		 * @param {Object} index
		 */
		validateFromItem(formKey, index, set = []) {
			/*验证表单数据*/
			return new Promise((resolve, reject) => {
				let form = index >= 0 ? this.$refs[formKey][index] : this.$refs[formKey];
				// let key = this.handleValidateMsg(form, index, formKey,set);
				// if (key) {
				// 	reject({formKey,index,errInfo:set});
				// } else {}
				if (form) {
					form.validate().then(res => {
						/*如果formkey有值返回false*/
						if (this.errorMsg[formKey] && Object.keys(this.errorMsg[formKey]).length > 0) {
							reject({
								formKey,
								index,
								errInfo: set
							});
						} else {
							if (index >= 0) {
								set[index] = true;
							}
							resolve(true);
						}
					}, err => {
						this.handleValidateMsg(form, index, formKey, set);
						reject({
							formKey,
							index,
							errInfo: set
						});
					})
				} else {
					resolve(true);
				}



			})
		},
		/**
		 * 单个字段校验处理
		 * @param {Object} formKey
		 * @param {Object} fieldName
		 * @param {Object} index
		 */
		focusValidateByFieldName(formKey, fieldName, index) {
			let form = index >= 0 ? this.$refs[formKey][index] : this.$refs[formKey];
			form.focusValidateByFieldName(fieldName);
		},
		/**
		 * 异常消息回调
		 * @param {Object} form
		 * @param {Object} index
		 * @param {Object} formKey
		 */
		handleValidateMsg(form, index, formKey, set) {
			let rules = form.rules;
			//console.log('rules',rules)
			let rulesKey = Object.keys(rules);
			let key = rulesKey.find(item => {
				return rules[item].message;
			});
			if (key) {
				let result = {
					index: index,
					message: rules[key].message
				};
				if (this.errorMsg.hasOwnProperty(formKey)) {
					let keysLength = Object.keys(this.errorMsg[formKey]).length;
					if (keysLength == 0) {
						this.errorMsg[formKey] = result;
					}
				} else if (!this.errorMsg.hasOwnProperty(formKey)) {
					this.errorMsg[formKey] = result;
				}
				set[index] = false;
			} else {
				set[index] = true;
			}
			return key;
		},
		/*表单提交前进行预处理*/
		beforeForm() {
			let self = this;
			self.errorMsg = {};
			self.scrollInto = '';
			let validateFroms = [];
			let validateFormsFiles = [];
			Object.keys(this.forms).forEach((form, i) => {
				/*是否包含附件*/
				if (this.config[form].hasOwnProperty('uploadFile')) {
					if (!Array.isArray(this.forms[form]) || Array.isArray(this.forms[form]) && this.forms[form].length > 0) {
						let uploadKey = `uploadFile_${form}`;
						/*判断附件是否必填*/
						let uploadRef = this.$refs[uploadKey];
						// console.log("uploadRef", uploadRef)
						if (Array.isArray(uploadRef)) {
							uploadRef.forEach((file, index) => {
								let {
									required,
									imgList,
									title,
									enable,
								} = uploadRef[index];
								if (required) {
									setTimeout(res => {
										self.errorMsg[`${form}_${i}`] = {
											message: self.$pt(self.$t('qscLocal.common.will_pass'), title)
										}
									})
								}
								if (enable) {
									validateFormsFiles.push({
										fileKey: uploadKey,
										name: form
									});
								}
							})
						} else {
							if (uploadRef && (uploadRef.length - 1 >= i)) {
								let {
									required,
									imgList,
									title,
									enable,
								} = uploadRef[index];
								if (required) {
									setTimeout(res => {
										self.errorMsg[`${form}_${i}`] = {
											message: self.$pt(self.$t('qscLocal.common.will_pass'), title)
										}
									})
								}
								if (enable) {
									validateFormsFiles.push({
										fileKey: uploadKey,
										name: form
									});
								}
							}
						}
						// if (uploadRef && (uploadRef.length - 1 >= i)) {

						// }
					}
				}
				if (Array.isArray(this.forms[form])) {
					let set = [];
					if (this.$refs[form]) {
						this.forms[form].forEach((item, index) => {
							validateFroms.push(this.validateFromItem(form, index, set));
						})
					}
				} else {
					validateFroms.push(this.validateFromItem(form, -1));
				}
			})
			return {
				validateFroms: validateFroms,
				validateFormsFiles: validateFormsFiles
			};
		},
		upploadFile(validateFormsFiles) {
			let self = this;
			let files_ = [];
			//处理附件
			console.log("validateFormsFiles=====22==",validateFormsFiles)
			validateFormsFiles.forEach(file => {
				let {
					fileKey,
					name
				} = file;
				if (self.$refs[file.fileKey]) {
					self.$refs[file.fileKey].forEach((item, filexIndex) => {
						let imagesList = item.imgList;
						let index = item.index;
						let {
							storageType,
							value
						} = self.config[name]['uploadFile'][index];
						let formItem;
						// 处理多个附件时，"enable": false的附件formItem:underfined报错问题
						if (Array.isArray(self.forms[name])) {
							if(self.forms[name][filexIndex]){
								formItem = self.forms[name][filexIndex];
							}
						} else {
							formItem = self.forms[name];
						}
						if(formItem){
							files_.push(new Promise((resolve, reject) => {
								self.$upload(imagesList, formItem[value] ? formItem[value] : false, storageType).then(result => {
									formItem[value] = result.fileGroupId ? result.fileGroupId : '';
									resolve(result.fileGroupId)
								})
							}))	
						}
					})
				}
			})
			return files_;
		},
		/**
		 * 定位显示错误信息
		 */
		showErrMsg() {
			let self = this;
			let errorMsg = self.errorMsg;
			setTimeout(() => {
				let errMsg = {};
				for (let key in errorMsg) {
					if (Object.keys(errMsg).length == 0) {
						let {
							message,
							index
						} = errorMsg[key];
						if (message) {
							errMsg = {
								message,
								key,
								index
							};
						}
					} else {
						break;
					}
				}
				let {
					message,
					key,
					index
				} = errMsg;
				if (index != -1) {
					self.scrollInto = `${key}_${index}`;
				} else {
					self.scrollInto = key;
				}
				console.log('self.scrollInto',self.scrollInto)
				self.chooseTab(self.scrollInto);
				self.$show(message);
			})
		}
	}
}
