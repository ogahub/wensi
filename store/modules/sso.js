
import API_SSO from '../../api/sso'
import Token from "../../common/token.js"
import Storage from "../../common/storage.js"

const sso ={
    namespaced: true,
    state:{
        count:0,
        token:Token.getToken(),
        ticket:''
    },
    getters:{

    },
    mutations:{
        addcount(state) {
            state.count ++
        },
        pushval(state,val) {
            state.count = val
        },
        setToken(state,token) {
            state.token = token
        }
    },
    actions:{
        pushcount({commit},val) {
           commit('pushval', val)
        },
        getLoginInfo({commit}) {
            let params = {
                Authorization:Token.getToken()
            }
            return API_SSO.getLoginInfo(params)
        },
        sysParams({commit},params) {
            return API_SSO.getLoginSso(params)
        },
        getTicketInfo({commit},params) {
			return API_SSO.getTicketInfo(params)
            
        }
    }
}

export default  sso;