import API_YERNEWS from '../../api/yernews.js'
const yernews = {
	namespaced: true,
	state: {
		age:'12'
	},
	getters: {
	
	},
	mutations: {
	
	},
	actions: {
		
		getTypelist({ commit }, param) {
			return API_YERNEWS.typelist(param)
		},

		getTypepage({ commit }, param) {
			return API_YERNEWS.typepage(param)
		},
		
		getMarkread({ commit }, id) {
			return API_YERNEWS.markread(id)
		},
		//公告
		getNoticelist({ commit }, param) {
			return API_YERNEWS.noticelist(param)
		},
		
		getNoticeget({ commit }, id) {
			return API_YERNEWS.noticeget(id)
		}
	},
}
export default yernews;