import API_USERS from "../../api/user.js"
import Token from "../../common/token.js"
import Storage from "../../common/storage.js"
const user = {
    namespaced: true,
    state: {
		authStatus:false,
        openId: "",
        token: Token.getToken(),
        
    },
    getters: {
        openId: (state) => state.openId,
		authStatus:(state) => state.authStatus,
    },    
    mutations: {
		setAuthStatus: (state, flag) => {
			console.log('mutations888',state, flag)
            // state.authStatus = flag
			Storage.setCache('authStatus', flag)
			// Storage.setCache('authStatus11', flag)
			// Storage.clearCache()
        },
        //设置openId
        SET_openId: (state, openId) => {
            state.openId = openId
        },
        //设置token
        SET_token: (state, token) => {
            state.token = token
        },
        
        //设置菜单
        setRoleList(state, roleList) {
            state.roleList = roleList
        },

        //退出登录
        logout(state) {
            console.log("退出", state)
            state.token = ""
            Token.setToken("")
            state.userInfo = {
                nickName: "", //昵称
                avatarUrl: state.userInfo.avatarUrl, //头像
                userCode: "", //账号
                userId: "", //用户id
                erpCode: "", //供应商编号
                erpSubAccount: "", //是否子账号
                userName: "", //姓名
                roleType: "", //角色类型
                mobile: "", //手机号
                email: "", //邮箱
                company: "", //公司
                authorities: [], //权限菜单
                loginedFlag: true, //首次登录标识
                fLogin: false, //是否开启人脸识别
                realUserName: "", //真实姓名
                idCard: "", //身份证号
                isOverFaceTime: false, //是否超过设置扫脸频率
            }
            state.wxLogin = false
            state.hasBind = false
        }    
    },
    actions: {
        //获取openId
        getOpenId({ commit, dispatch }, param) {
			
            uni.login({
            	provider: 'weixin',
            	success: (res2) => {
					console.log('store_user889')
            		uni.getUserInfo({
            			provider: 'weixin',
            			success: (info) => { 
            				console.log(3,'获取code然后调接口获取openid', res2);
            				console.log(4,'获取昵称等', info);
            				
            			},
            			fail: () => {
            				uni.showToast({
            					title: "微信登录授权失败",
            					icon: "none"
            				});
            			}
            		})
            				
            	},
            	fail: () => {
            		uni.showToast({
            			title: "微信登录授权失败",
            			icon: "none"
            		});
            	}
            })
        },
        // 绑定登录
        bindLogin({ commit, state }, loginInfo) {
            const { userCode, password } = loginInfo
            return new Promise((resolve, reject) => {
                API_USERS.bindOpenId({ username: userCode, password: password, openId: state.openId })
                    .then((res) => {
                        let { success, data } = res
                        if (success) {
                            if (data.fLogin) {
                                let objStr = JSON.stringify(data)
								console.log(11,objStr)
                                if (data.idCard && data.realUserName) {
                                    uni.navigateTo({
                                        url: "/pages/users/faceRecognition?objStr=" + objStr,
                                    })
                                } else {
                                    uni.navigateTo({
                                        url: "/pages/users/personInfor?objStr=" + objStr,
                                    })
                                }
                            } else {
                                commit("setBind", true)
                                commit("set_Info", data)

                                let arr = state.applyList.filter((v) => state.userInfo.authorities.indexOf(v.type) != -1)
                                commit("setRoleList", arr)

                                commit("SET_token", data.token)
                                Token.setToken(data.token)

                                resolve()
                            }
                        } else if (data.errcode == 500) {
                            console.log(data.errmsg)
                            uni.showToast({
                                title: data.errmsg,
                                duration: 1000,
                                icon: "none",
                            })
                        }
                    })
                    .catch((error) => {
                        reject(error)
                    })
            })
        },
        //通过openId登录
        login({ commit, state }, obj) {
            console.log("key", obj.openId, obj.key)
            API_USERS.login({ openId: obj.openId })
                .then((res) => {
            		
                    let { success, data } = res
                    if (success) {
                        if (!data.fLogin) { //是否开启人脸
                            let objStr = JSON.stringify(data)
                            if (data.idCard && data.realUserName) { //是否录入身份信息
                                if (obj.key) { //存在-进入小程序登录; 不存在-授权登录
                                    if (data.isOverFaceTime) { //是否超过免扫时间限制
                                        uni.navigateTo({
                                            url: "/pages/users/faceRecognition?objStr=" + objStr,
                                        })
                                    } else {
                                        //存在-进入小程序登录--直接登陆
                                        //已绑定
                                        commit("setBind", true)
                                        commit("set_Info", data)
            
                                        //设置菜单
                                        let arr = state.applyList.filter((v) => state.userInfo.authorities.indexOf(v.type) != -1)
                                        commit("setRoleList", arr)
            
                                        commit("SET_token", data.token)
                                        Token.setToken(data.token)
            
                                        //判断是否首次登陆
                                        if (data.loginedFlag) {
                                            uni.navigateTo({
                                                url: "/pages/users/changePwd",
                                            })
                                        }
                                    }
                                } else {
                                    uni.navigateTo({
                                        url: "/pages/users/faceRecognition?objStr=" + objStr,
                                    })
                                }
                            } else {
                                uni.navigateTo({
                                    url: "/pages/users/personInfor?objStr=" + objStr,
                                })
                            }
                        } else {
                            //已绑定
                            commit("setBind", true)
                            commit("set_Info", data)
            
                            //设置菜单
                            let arr = state.applyList.filter((v) => state.userInfo.authorities.indexOf(v.type) != -1)
                            commit("setRoleList", arr)
            
                            commit("SET_token", data.token)
                            Token.setToken(data.token)
            
                            //判断是否首次登陆
                            if (data.loginedFlag) {
                                uni.navigateTo({
                                    url: "/pages/users/changePwd",
                                })
                            }
                        }
                    }
            
                    resolve()
                })
                .catch((error) => {
                    //清空缓存数据
                    commit("deleteData")
            
                    setTimeout(() => {
                        uni.navigateTo({
                            url: "/pages/users/login?key=0&lastPage=1",
                        })
                    }, 0)
                    reject(error)
                })
        },
        //修改密码
        changePwd({ commit }, param) {
            return API_USERS.changePwd(param)
        },
        //联系我们
        contact({ commit }, param) {
            return API_USERS.contact(param)
        },
        //操作手册下载
        downLoadFile({ commit }, param) {
            return API_USERS.downLoadFile(param)
        },
        //注册
        register({ commit }, param) {
            return API_USERS.register(param)
        },
        //注册校验
        registerVerify({ commit }, param) {
            return API_USERS.registerVerify(param)
        },
    },
}

export default user
