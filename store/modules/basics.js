import API_BASICS from "../../api/basics.js"
const basics = {
	namespaced: true,
	state: {
		indexTabIndex: 0,
		totaldata:[],
		dictionary:{},
	},
	getters: {
	
	},
	mutations: {
		SET_indexTabIndex: (state, index) => {
		    state.indexTabIndex = index
		},
	},
	actions: {
		
		getTypelist({ commit }, param) {
			return API_YERNEWS.typelist(param)
		},
	
		getWorkbenchList({ commit }, param) {
		    return API_BASICS.workbenchList(param)
		},
		purGet({ commit }, param) {
		    return API_BASICS.purchasingrequisitionGet(param)
		},
		purGetdetail({ commit }, param) {
		    return API_BASICS.purchasingrequisitionGetdetail(param)
		},
		purchasingtopass({ commit }, param) {
		    return API_BASICS.purchasingtopass(param)
		},
		purchasingtonopass({ commit }, param) {
		    return API_BASICS.purchasingtonopass(param)
		},
		purchasinggetall({ commit }, param) {
		    return API_BASICS.purchasinggetall(param)
		},
		// 询价
		getinquirynoticedtl({ commit }, param) {
		    return API_BASICS.getinquirynoticedtl(param)
		},
		getinquirymatdtl({ commit }, param) {
		    return API_BASICS.getinquirymatdtl(param)
		},
		getinquiryvendordtl({ commit }, param) {
		    return API_BASICS.getinquiryvendordtl(param)
		},
		getbaseInfo({ commit }, param) {
		    return API_BASICS.getbaseInfo(param)
		},
		inquirytopass({ commit }, param) {
		    return API_BASICS.inquirytopass(param)
		},
		inquirytonopass({ commit }, param) {
		    return API_BASICS.inquirytonopass(param)
		},
		//供应商
		getvendorin({ commit }, param) {
		    return API_BASICS.getvendorin(param)
		},
		getvendorincapacityindtl({ commit }, param) {
		    return API_BASICS.getvendorincapacityindtl(param)
		},
		getvendorinqualityindtl({ commit }, param) {
		    return API_BASICS.getvendorinqualityindtl(param)
		},
		
		getvendorincompanyindtl({ commit }, param) {
		    return API_BASICS.getvendorincompanyindtl(param)
		},
		getvendorinmaterialgroupindtl({ commit }, param) {
		    return API_BASICS.getvendorinmaterialgroupindtl(param)
		},
		getvendorinporgindtl({ commit }, param) {
		    return API_BASICS.getvendorinporgindtl(param)
		},
		getvendorinauthindtl({ commit }, param) {
		    return API_BASICS.getvendorinauthindtl(param)
		},
		getvendorinbankindtl({ commit }, param) {
		    return API_BASICS.getvendorinbankindtl(param)
		},
		vendortopass({ commit }, param) {
		    return API_BASICS.vendortopass(param)
		},
		vendortonopass({ commit }, param) {
		    return API_BASICS.vendortonopass(param)
		},
		// 合同
		getcontract({ commit }, param) {
		    return API_BASICS.getcontract(param)
		},
		getcontractMaterial({ commit }, param) {
		    return API_BASICS.getcontractMaterial(param)
		},
		getcontractContent({ commit }, param) {
		    return API_BASICS.getcontractContent(param)
		},
		getContractField({ commit }, param) {
		    return API_BASICS.getContractField(param)
		},
		// 参数：filter: "(contractNo eq 'HT2021072600009') "
		getContractSignFile({ commit }, param) {
		    return API_BASICS.getContractSignFile(param)
		},
		getContractFile({ commit }, param) {
		    return API_BASICS.getContractFile(param)
		},
		contracttopass({ commit }, param) {
		    return API_BASICS.contracttopass(param)
		},
		contracttonopass({ commit }, param) {
		    return API_BASICS.contracttonopass(param)
		},
		// 文件处理
		viewFile({ commit }, param) {
		    return API_BASICS.viewFile(param)
		},
		download({ commit }, param) {
		    return API_BASICS.download(param)
		},
		
		// 直采-单
		
		// 直采—定价单
		directpurchaseget({ commit }, param) {
		    return API_BASICS.directpurchaseget(param)
		},
		directpurchasegetdetail({ commit }, param) {
		    return API_BASICS.directpurchasegetdetail(param)
		},
		directpurchasetopass({ commit }, param) {
		    return API_BASICS.directpurchasetopass(param)
		},
		directpurchasetonopass({ commit }, param) {
		    return API_BASICS.directpurchasetonopass(param)
		},
	
	},
    
}
export default basics;
