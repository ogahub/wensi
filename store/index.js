import Vue from 'vue'
import Vuex from 'vuex'

import user from '@/store/modules/user.js'
import yernews from '@/store/modules/yernews.js'
import basics from '@/store/modules/basics.js'
import sso from '@/store/modules/sso.js'

// import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

// const vuexPersisted = new createPersistedState({
// 	storage: {
// 		getItem: key => uni.getStorageSync(key),
// 		setItem: (key, value) => uni.setStorageSync(key, value),
// 		removeItem: key => uni.removeStorageSync(key)
// 	}
// })

const store = new Vuex.Store({
	modules: {
		user,
		yernews,
		basics,
		sso
	},

	// plugins: [vuexPersisted]
})

export default store

