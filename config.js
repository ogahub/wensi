module.exports = {
	
	// 文思测试环境
	// baseUrl: 'http://10.111.66.140',
	// 文思正式环境
	baseUrl: 'https://srm.pactera.com',
	// 开发环境
	// baseUrl: 'http://10.10.21.35',
	// 质检单状态
	statusMap: [
	    { label: "新建", value: "NEW" },
	    { label: "待审核", value: "CONFIRM" },
	    { label: "供应商驳回", value: "VENDORREJECT" },
	    { label: "待签订", value: "UNSING" },
	    { label: "驳回", value: "REJECT" },
	    { label: "待审核过期", value: "CONFIRMOVER" },
	    { label: "WMS2质检", value: "WMS2WAITQUALITY" },
	    { label: "接受", value: "ACCEPT" },
	    { label: "发布", value: "RELEASE" },
	    { label: "执行", value: "OPEN" },
	    { label: "关闭", value: "CLOSE" },
	    { label: "取消", value: "CANCEL" },
	],
}
