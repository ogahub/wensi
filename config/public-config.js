/*白名单处理*/
export const WHITE_URL_DATA = ['locale', 'userlogin', 'finddictionary', 'register/getsmscode', 'register/validatesmscode',
		'register/save', 'forgotpassword/getsmscode',
		'forgotpassword/validatesmscode', 'forgotpassword/validatesmscode', 'forgotpassword/sendemail',
		'forgotpassword/reset', 'opversion'
	];
/*加密公钥匙**/
export const  PUBLIC_KEY  = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCCV88D7F/aUTvB9F0ZHhGk++9MPHblXc8zIrj2HXWBH/xgNzqZUPhinzgKFmJQE5EZydQbv8BW0iPoHjYPe0Zo/MHzJkvniaMhkYej2ii1tuQm411e9KLCR19mZqc1b96O3PGRcM5t0GZxt+ZzDX17+S02l58KkiNrELfLtJDa7QIDAQAB";
